# Sports Centers Management Application

This repository contains sports centers management Application. Application was developed in Spring Framework. Project was part of my diploma exam.

**Features:** <hr>
- Sports center clients can register account in application, 
- Creating and management workers, clients and admins accounts, 
- Users premissions management, 
- Passes managament, 
- Rooms management in sport center, 
- Activities timetables management, 
- Online sport activities reservation by clients, 
- Content management system for sports center website, 
- Tickets and payments management (Clients can order tickets) 
- Auto removing sport activities in case of out of date time 

**Used technology:** <hr>
- Spring framework, spring boot, spring security, JST, mysql, CORS 

**First run instructions:**<hr>
1. Go to: src\main\resources\application.properties
Edit data base connection parameters (username,password,port,data base name, etc)
2. Run application, opern browser and go to: http://localhost:8080/
3. Try to login to application by this data:<br />
username:	admin <br />
password: 	fjgjtg457kfde4<br />
<b>Important!</b> Create your own admin account after login and delete created all default users!
4. You can manage init data by editing src\main\java\zut\wi\Sportscentersmanagement\Components\InitData.java


Screens:<hr> <br />
<img src="https://i.ibb.co/4MJ2t1r/1.png" alt="1" border="0">

<img src="https://i.ibb.co/7YPQKDv/2.png" alt="2" border="0">

<img src="https://i.ibb.co/vkkbxFZ/3.png" alt="3" border="0">

<img src="https://i.ibb.co/C9RhF9h/4.png" alt="4" border="0">

<img src="https://i.ibb.co/6HS704C/5.png" alt="5" border="0">

**Bugs:**<hr><br />
1.  Bad expiration date/time format in actual orders list. For example: 2019-06-03 13:36:02.0 (should be 2019-06-03). This case is only in user interface.
2.  When some data are deleting by button in application if you click previous site button in browser you get HTTP 500 error. 
3.  You tell me :)