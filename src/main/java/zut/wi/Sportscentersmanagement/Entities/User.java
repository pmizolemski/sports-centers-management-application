package zut.wi.Sportscentersmanagement.Entities;
import javax.persistence.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @Size(min = 4, max = 30)
    private String nick;
    @NotNull
    @Size(min = 4, max = 30)
    private String name;
    @NotNull
    @Size(min = 4, max = 30)
    private String surname;
    @NotNull
    @Email
    @Size(min = 4, max = 30)
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String passwordConfirm;
    @NotNull
    @Size(min = 4, max = 30)
    private String address;
    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public User(Set<Role> roles) {
        this.roles = roles;
    }

    public User() {

    }

    public User(@NotNull @Size(min = 4, max = 30) String nick,
                @NotNull @Size(min = 4, max = 30) String name,
                @NotNull @Size(min = 4, max = 30) String surname,
                @NotNull @Email @Size(min = 4, max = 30) String email,
                @NotNull String password, @NotNull String passwordConfirm,
                @NotNull @Size(min = 4, max = 30) String address) {
        this.nick = nick;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }


    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
