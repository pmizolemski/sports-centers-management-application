package zut.wi.Sportscentersmanagement.Entities;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Settings {
    @Id
    private int id;
    private boolean firstRunApplication; //for save init data to database

    public Settings(int id,boolean firstRunApplication) {
        this.firstRunApplication = firstRunApplication;
        this.id=id;
    }

    public Settings() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFirstRunApplication() {
        return firstRunApplication;
    }

    public void setFirstRunApplication(boolean firstRunApplication) {
        this.firstRunApplication = firstRunApplication;
    }
}
