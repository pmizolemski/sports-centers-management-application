package zut.wi.Sportscentersmanagement.Entities;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class WebsiteContent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    private String contentType;
    @NotNull
    @Column(columnDefinition="LONGTEXT")
    private String shortDescription;
    @NotNull
    @Column(columnDefinition="LONGTEXT")
    private String longDescription;
    private String imageName;
    private String url;
    @NotNull
    private String title;

    public WebsiteContent() {
    }

    public WebsiteContent(@NotNull String contentType, @NotNull String shortDescription,
                          @NotNull String longDescription, String imageName, String url,
                          @NotNull String title) {
        this.contentType = contentType;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.imageName = imageName;
        this.url = url;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}


