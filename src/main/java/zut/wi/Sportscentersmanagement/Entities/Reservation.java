package zut.wi.Sportscentersmanagement.Entities;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    @OneToOne
    private Activity activityType;
    @NotNull
    @OneToOne
    private Room room;
    @NotNull
    @OneToOne
    private User leader;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date  reservedDate;
    @NotNull
    @DateTimeFormat(pattern="kk:mm")
    private Date reservedFromHour;
    @DateTimeFormat(pattern="kk:mm")
    private  Date reservetToHour;
    private Integer numberOfUsers;
    public boolean isReservationAvaliable;

    public Reservation() {
    }

    public int getId() {
        return id;
    }

    public String activityString() {
        return activityType.getType();
    }

    public void setId(int id) {
        this.id = id;
    }

    public Activity getActivityType() {
        return activityType;
    }

    public void setActivityType(Activity activityType) {
        this.activityType = activityType;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Date getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(Date reservedDate) {
        this.reservedDate = reservedDate;
    }

    public Date getReservedFromHour() {
        return reservedFromHour;
    }

    public void setReservedFromHour(Date reservedFromHour) {
        this.reservedFromHour = reservedFromHour;
    }

    public Date getReservetToHour() {
        return reservetToHour;
    }

    public void setReservetToHour(Date reservetToHour) {
        this.reservetToHour = reservetToHour;
    }

    public User getLeader() {
        return leader;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    public Integer getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(Integer numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    public boolean isReservationAvaliable() {
        return isReservationAvaliable;
    }

    public void setReservationAvaliable(boolean reservationAvaliable) {
        isReservationAvaliable = reservationAvaliable;
    }
}
