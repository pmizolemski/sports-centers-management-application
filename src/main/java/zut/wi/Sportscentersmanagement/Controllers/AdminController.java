package zut.wi.Sportscentersmanagement.Controllers;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zut.wi.Sportscentersmanagement.Components.ValidationComponent;
import zut.wi.Sportscentersmanagement.Entities.*;
import zut.wi.Sportscentersmanagement.Models.SearchForm;
import zut.wi.Sportscentersmanagement.Repositories.ReservationRepository;
import zut.wi.Sportscentersmanagement.Repositories.RoleRepository;
import zut.wi.Sportscentersmanagement.Repositories.UsersRepository;
import zut.wi.Sportscentersmanagement.Services.AdminService;
import zut.wi.Sportscentersmanagement.Services.UserServiceManager;
import zut.wi.Sportscentersmanagement.Services.WebsiteContentService;

@Controller
@CrossOrigin(origins = "*")
public class AdminController {
    @Autowired
    private UserServiceManager userServiceManager;
    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ValidationComponent validationComponent;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private WebsiteContentService websiteContentService;
    private static final Logger LOG = Logger.getLogger(AdminController.class);

    @RequestMapping(value = "/admin/manageUsers", method = RequestMethod.GET)
    public String showAllUsers(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/manageUsers, method: GET"));
        adminService.showAllUsers(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageUsers";
    }

    @RequestMapping(value = "/admin/deleteUser/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer id, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/deleteUser/{id}, method: GET"));
        adminService.deleteUser(id, model);
        websiteContentService.loadPageContent(model);
        return "admin_manageUsers";
    }

    @RequestMapping(value = "/admin/searchUser", method = RequestMethod.POST)
    public String registration(@ModelAttribute("searchForm") SearchForm searchForm, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/searchUser}, method: POST"));
        adminService.searchUser(searchForm,model);
        websiteContentService.loadPageContent(model);
        return "admin_resultSearch";
    }

    @RequestMapping(value="admin/premissions", method=RequestMethod.GET)
    public String premissions(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/premissions}, method: GET"));
        adminService.showPremissions(model);
        websiteContentService.loadPageContent(model);
        return "admin_premissions";
    }

    @RequestMapping(value = "admin/updateRole/{userId}/{roleId}", method = RequestMethod.GET)
    public String updateRole (@PathVariable Integer userId, @PathVariable Integer roleId, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/pdateRole/{userId}/{roleId}}, method: GET"));
        adminService.updateRole(userId,roleId,model);
        websiteContentService.loadPageContent(model);
        return "admin_premissions";
    }

    @RequestMapping(value="admin/passes", method=RequestMethod.GET)
    public String managePasses(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/passes, method: GET"));
        adminService.managePasses(model);
        websiteContentService.loadPageContent(model);
        return "admin_managePasses";
    }

    @RequestMapping(value="admin/pass/edit/{passId}", method=RequestMethod.GET)
    public String showPassToEdit(@PathVariable Integer passId, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/pass/edit/{passId}, method: GET"));
        managePasses(model);
        adminService.showPassToEdit(passId,model);
        websiteContentService.loadPageContent(model);
        return "admin_managePasses";
    }

    @RequestMapping(value="admin/pass/edit/", method=RequestMethod.POST)
    public String editPass (Model model,@ModelAttribute("passForm") Pass passForm,BindingResult bindingResult) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin//pass/edit/}, method: POST"));
        validationComponent.validatePass(passForm,bindingResult);
        managePasses(model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isPassEditMode",true);
            websiteContentService.loadPageContent(model);
            return "admin_managePasses";
        }
        else {
            adminService.editPass(passForm, model);
            websiteContentService.loadPageContent(model);
            return "admin_managePasses";
        }
    }

    @RequestMapping(value = "/admin/pass/delete/{id}", method = RequestMethod.GET)
    public String deletePass(@PathVariable Integer id, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/pass/delete/{id}, method: GET"));
        adminService.deletePass(id,model);
        websiteContentService.loadPageContent(model);
        return "admin_managePasses";
    }

    @RequestMapping(value = "/admin/pass/add", method = RequestMethod.GET)
    public String addPass(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/pass/add, method: GET"));
        adminService.addPass(model);
        websiteContentService.loadPageContent(model);
        return "admin_managePasses";
    }

    @RequestMapping(value = "/admin/pass/manageUsersPasses", method = RequestMethod.GET)
    public String manageUsersPasses(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin//pass/manageUsersPasses, method: GET"));
        adminService.manageUsersPasses(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageUsersPasses";
    }

    @RequestMapping(value = "/admin/showUser/{userId}", method = RequestMethod.GET)
    public String showUser(@PathVariable Integer userId,Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin//showUser/{userId}, method: GET"));
        adminService.showUser(userId,model);
        websiteContentService.loadPageContent(model);
        return "user_updateProfile";
    }

    @RequestMapping(value = "admin/showUser/{userId}", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute("userForm") User userForm,@PathVariable Integer userId, BindingResult bindingResult, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/showUser/{userId}, method: POST"));
        validationComponent.setEditedProfile(true);
        validationComponent.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            websiteContentService.loadPageContent(model);
            return "user_updateProfile";
        }
        websiteContentService.loadPageContent(model);
        adminService.updateUserProfile(userForm,userId);
        adminService.showAllUsers(model);
        return "admin_manageUsers";
    }

    @RequestMapping(value = "admin/pass/payments", method = RequestMethod.GET)
    public String showPayments(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/pass/payments/, method: GET"));
        adminService.showPayments(model);
        websiteContentService.loadPageContent(model);
        return "admin_payments";
    }

    @RequestMapping(value = "/admin/payment/delete/{paymentId}", method = RequestMethod.GET)
    public String deletePayment(@PathVariable Integer paymentId, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/admin/payment/delete/{payment.id}, method: GET"));
        adminService.deletePayment(paymentId,model);
        websiteContentService.loadPageContent(model);
        return "admin_payments";
    }

    @RequestMapping(value = "/admin/payment/confirm/{paymentId}", method = RequestMethod.GET)
    public String confirmPayment(@PathVariable Integer paymentId, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/admin/payment/delete/{payment.id}, method: GET"));
        adminService.confirmPayment(paymentId,model);
        websiteContentService.loadPageContent(model);
        return "admin_payments";
    }

    @RequestMapping(value = "/admin/setPass", method = RequestMethod.GET)
    public String setPass(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        adminService.setPass(model);
        websiteContentService.loadPageContent(model);
        return "admin_setPass";
    }

    @RequestMapping(value = "/admin/setPass/{passId}/{userNick}", method = RequestMethod.GET)
    public String setPass(@PathVariable Integer passId, @PathVariable String userNick,Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        userServiceManager.orderPass(passId,userNick,model,true);
        adminService.setPass(model);
        websiteContentService.loadPageContent(model);
        return "admin_setPass";
    }

    @RequestMapping(value="admin/activities", method=RequestMethod.GET)
    public String manageActivities(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/activities, method: GET"));
        adminService.manageActivities(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageActivities";
    }

    @RequestMapping(value="admin/activity/edit/{activityId}", method=RequestMethod.GET)
    public String showActivityToEdit(@PathVariable Integer activityId, Model model) {
        if((adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false))
            return "noPremissions";
        LOG.info(String.format("request admin/activity/edit/{activityId}, method: GET"));
        manageActivities(model);
        adminService.showActivityToEdit(activityId,model);
        websiteContentService.loadPageContent(model);
        return "admin_manageActivities";
    }

    @RequestMapping(value="admin/activity/edit/", method=RequestMethod.POST)
    public String editActivity (Model model, @ModelAttribute("activityForm") Activity activityForm, BindingResult bindingResult) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin//activity/edit/}, method: POST"));
        validationComponent.validateActivity(activityForm,bindingResult);
        manageActivities(model);
        websiteContentService.loadPageContent(model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isActivityEditMode",true);
            return "admin_manageActivities";
        }
        else {
            adminService.editActivity(activityForm, model);
            manageActivities(model);
            return "admin_manageActivities";
        }
    }

    @RequestMapping(value = "/admin/activity/delete/{id}", method = RequestMethod.GET)
    public String deleteActivity(@PathVariable Integer id, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/activity/delete/{id}, method: GET"));
        adminService.deleteActivity(id,model);
        websiteContentService.loadPageContent(model);
        return "admin_manageActivities";
    }

    @RequestMapping(value = "/admin/activity/add", method = RequestMethod.GET)
    public String addActivity(Model model) {
        if((adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false))
            return "noPremissions";
        LOG.info(String.format("request admin/activity/add, method: GET"));
        adminService.addActivity(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageActivities";
    }

    @RequestMapping(value="admin/rooms", method=RequestMethod.GET)
    public String manageRooms(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/manageRooms, method: GET"));
        adminService.manageRooms(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageRooms";
    }

    @RequestMapping(value="admin/room/edit/{roomId}", method=RequestMethod.GET)
    public String showRoomToEdit(@PathVariable Integer roomId, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/room/edit/{roomId}, method: GET"));
        manageRooms(model);
        adminService.showRoomToEdit(roomId,model);
        websiteContentService.loadPageContent(model);
        return "admin_manageRooms";
    }

    @RequestMapping(value="admin/room/edit/", method=RequestMethod.POST)
    public String editRoom (Model model, @ModelAttribute("roomForm") Room roomForm, BindingResult bindingResult) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin//room/edit/}, method: POST"));
        validationComponent.validateRoom(roomForm,bindingResult);
        websiteContentService.loadPageContent(model);
        manageRooms(model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isRoomEditMode",true);
            return "admin_manageRooms";
        }
        else {
            adminService.editRoom(roomForm, model);
            manageRooms(model);
            return "admin_manageRooms";
        }
    }

    @RequestMapping(value = "/admin/room/delete/{id}", method = RequestMethod.GET)
    public String deleteRoom(@PathVariable Integer id, Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/room/delete/{id}, method: GET"));
        adminService.deleteRoom(id,model);
        websiteContentService.loadPageContent(model);
        return "admin_manageRooms";
    }

    @RequestMapping(value = "/admin/room/add", method = RequestMethod.GET)
    public String addRoom(Model model) {
        if(!adminService.hasLoggedUserPremissions("ROLE_ADMIN"))
            return "noPremissions";
        LOG.info(String.format("request admin/room/add, method: GET"));
        adminService.addRoom(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageRooms";    }

    @RequestMapping(value="admin/reservations", method=RequestMethod.GET)
    public String manageReservations(Model model) {
        if((adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false))
            return "noPremissions";
        LOG.info(String.format("request admin/reservation, method: GET"));
        adminService.manageReservations(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageReservations";
    }

    @RequestMapping(value="admin/reservation/edit/", method=RequestMethod.POST)
    public String editReservation (Model model, @ModelAttribute("reservationForm") Reservation reservationForm,@RequestParam("room")Room room,
                                   @RequestParam("activityType") Activity activity,@RequestParam("leader") User leader,BindingResult bindingResult) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin//reservation/edit/}, method: POST"));
        reservationForm.setRoom(room);
        reservationForm.setActivityType(activity);
        reservationForm.setLeader(leader);
        validationComponent.validateReservation(reservationForm,bindingResult);
        manageReservations(model);
        websiteContentService.loadPageContent(model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isReservationEditMode",true);
            adminService.loadResources(model);
            return "admin_manageReservations";
        }
        else {
            adminService.editReservation(reservationForm, model);
            manageReservations(model);
            return "admin_manageReservations";
        }
    }

    @RequestMapping(value = "/admin/reservation/delete/{id}", method = RequestMethod.GET)
    public String deleteReservation(@PathVariable Integer id, Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/reservation/delete/{id}, method: GET"));
        adminService.deleteReservation(id,model);
        websiteContentService.loadPageContent(model);
        return "admin_manageReservations";
    }

    @RequestMapping(value = "/admin/reservation/add", method = RequestMethod.GET)
    public String addReservation(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("request admin/reservation/add, method: GET"));
        adminService.addReservation(model);
        websiteContentService.loadPageContent(model);
        return "admin_manageReservations";
    }
}