package zut.wi.Sportscentersmanagement.Controllers;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import zut.wi.Sportscentersmanagement.Services.HomeService;
import zut.wi.Sportscentersmanagement.Services.WebsiteContentService;

@Controller
@CrossOrigin(origins = "*")
public class HomeController {
    @Autowired
    private WebsiteContentService websiteContentService;
    @Autowired
    private HomeService homeControllerService;
    private static final Logger LOG = Logger.getLogger(WebsiteContentService.class);
    @RequestMapping("/")
    public String home(Model model) {
        LOG.info(String.format("FUNCTION BODY: home"));
        websiteContentService.loadMainMenu(model);
        websiteContentService.loadBoxes(model);
        websiteContentService.loadFooter(model);
        websiteContentService.loadSlider(model);
        websiteContentService.loadNews(model);
        return "home";
    }

    @RequestMapping (value = "/page/{page}", method = RequestMethod.GET)
    public  String loadFullPage (Model model, @PathVariable String page) {
        LOG.info(String.format("/page/{page}, method: GET"));
        websiteContentService.loadPageContent(model);
        homeControllerService.loadFullPage(model, page);
        return "SinglePage";
    }
}
