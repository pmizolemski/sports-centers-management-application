package zut.wi.Sportscentersmanagement.Controllers;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zut.wi.Sportscentersmanagement.Components.ValidationComponent;
import zut.wi.Sportscentersmanagement.Entities.User;
import zut.wi.Sportscentersmanagement.Services.SecurityServiceManager;
import zut.wi.Sportscentersmanagement.Services.UserServiceManager;
import zut.wi.Sportscentersmanagement.Services.WebsiteContentService;

@Controller
@CrossOrigin(origins = "*")
public class UserController {
    @Autowired
    private UserServiceManager userServiceManager;
    @Autowired
    private SecurityServiceManager securityServiceManager;
    @Autowired
    private ValidationComponent validationComponent;
    @Autowired
    private WebsiteContentService websiteContentService;
    private static final Logger LOG = Logger.getLogger(UserController.class);

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        LOG.info(String.format("request /registration, method: GET"));
        model.addAttribute("userForm", new User());
        websiteContentService.loadPageContent(model);
        return "user_registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        LOG.info(String.format("request /registration, method: POST"));
        validationComponent.validate(userForm, bindingResult);
        websiteContentService.loadPageContent(model);
        if (bindingResult.hasErrors()) {
            return "user_registration";
        }
        userServiceManager.setUserProfileEdited(false);
        userServiceManager.save(userForm);
        return "redirect:/user/update";
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.GET)
    public String updateProfile(Model model) {
        LOG.info(String.format("request /user/update, method: GET"));
        userServiceManager.updateUser(model);
        websiteContentService.loadPageContent(model);
        return "user_updateProfile";
    }

    @RequestMapping(value = "user/update", method = RequestMethod.POST)
    public String updateProfile(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        LOG.info(String.format("request /user/update, method: post"));
        validationComponent.setEditedProfile(true);
        validationComponent.validate(userForm, bindingResult);
        websiteContentService.loadPageContent(model);
        if (bindingResult.hasErrors()) {
            return "user_updateProfile";
        }
        userServiceManager.updateUserProfile(userForm);
        return "redirect:/";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        LOG.info(String.format("request /login, method: GET"));
        userServiceManager.login(error,model,logout);
        websiteContentService.loadPageContent(model);
        return "user_login";
    }

    @RequestMapping(value="user/passes", method = RequestMethod.GET)
    public String showAvaliablePasses(Model model) {
        LOG.info(String.format("request /user/passes, method: GET"));
        userServiceManager.showAvaliablePasses(model);
        websiteContentService.loadPageContent(model);
        return "user_passes";
    }

    @RequestMapping(value = "user/pass/order/{passId}/{userNick}", method = RequestMethod.GET)
    public String orderPass(@PathVariable Integer passId,@PathVariable String userNick, Model model) {
        LOG.info(String.format("request /user/pass/order/{passId}/{userNick}, method: GET"));
        userServiceManager.orderPass(passId,userNick,model,false);
        websiteContentService.loadPageContent(model);
        return "user_passes";
    }

    @RequestMapping(value = "user/pass/delete/{orderId}", method = RequestMethod.GET)
    public String deleteOrderPass(@PathVariable Integer orderId, Model model) {
        LOG.info(String.format("request /user/pass/delete/{orderId}, method: GET"));
        userServiceManager.deleteOrderPass(orderId,model);
        websiteContentService.loadPageContent(model);
        return "user_passes";
    }

    @RequestMapping(value = "user/reservations", method=RequestMethod.GET)
    public String reservations(Model model) {
        LOG.info(String.format("user/reservations, method: GET"));
        userServiceManager.reservation(model);
        websiteContentService.loadPageContent(model);
        return "user_reservations";
    }

    @RequestMapping(value = "user/reservation/book/{bookId}", method=RequestMethod.GET)
    public String bookReservation(Model model,@PathVariable Integer bookId) {
        LOG.info(String.format("user/reservation/book/bookId, method: GET"));
        userServiceManager.bookReservation(model,bookId);
        websiteContentService.loadPageContent(model);
        return "user_reservations";
    }

    @RequestMapping(value = "user/delete/booked/{reservationId}", method=RequestMethod.GET)
    public String deleteBookReservation(Model model,@PathVariable Integer reservationId) {
        LOG.info(String.format("/user/delete/booked/{reservationUserId}, method: GET"));
        userServiceManager.deleteBookReservation(model,reservationId);
        websiteContentService.loadPageContent(model);
        return "user_reservations";
    }
}
