package zut.wi.Sportscentersmanagement.Controllers;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zut.wi.Sportscentersmanagement.Components.ValidationComponent;
import zut.wi.Sportscentersmanagement.Entities.WebsiteContent;
import zut.wi.Sportscentersmanagement.Services.AdminService;
import zut.wi.Sportscentersmanagement.Services.WebsiteContentService;

@Controller
@CrossOrigin(origins = "*")
public class WebsiteContentController {
    private static final Logger LOG = Logger.getLogger(WebsiteContentController.class);
    @Autowired
    private WebsiteContentService websiteContentService;
    @Autowired
    private ValidationComponent validationComponent;
    @Autowired
    private AdminService adminService;

    @RequestMapping (value = "/cms/show", method = RequestMethod.GET)
    public  String showCMS (Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/cms/show, method: GET"));
        websiteContentService.showCMS(model);
        websiteContentService.loadPageContent(model);
        return "cmsManage";
    }

    @RequestMapping (value = "/cms/showContent/{contentId}", method = RequestMethod.GET)
    public String showContent(Model model,@PathVariable Integer contentId) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/cms/showContent/{contentId}, method: GET"));
        websiteContentService.showContent(contentId,model);
        showCMS(model);
        websiteContentService.loadPageContent(model);
        return "cmsManage";
    }

    @RequestMapping (value = "/cms/content/edit", method = RequestMethod.POST)
    public String addContent(Model model, @RequestParam("file") MultipartFile file, @ModelAttribute("contentForm") WebsiteContent contentForm,
                             @RequestParam("contentType") String contentType, BindingResult bindingResult){
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/cms/content/edit, method: POST"));
        validationComponent.validateNewContent(contentForm,contentType, bindingResult);
        websiteContentService.loadPageContent(model);
        if (bindingResult.hasErrors()) {
            websiteContentService.showCMS(model);
            model.addAttribute("isEditOrAddMode",true);
            return "cmsManage";
        }
        websiteContentService.addContent(model,contentForm,contentType,file);
        showCMS(model);
        return "cmsManage";
    }

    @RequestMapping (value = "/cms/deleteContent/{contentId}", method = RequestMethod.GET)
    public String deleteContent(Model model, @PathVariable Integer contentId) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        LOG.info(String.format("/cms/deleteContent/{contentId}, method: POST"));
        websiteContentService.deleteContent(model,contentId);
        websiteContentService.loadPageContent(model);
        return "cmsManage";
    }

    @RequestMapping (value = "cms/content/add", method = RequestMethod.GET)
    public String addContent(Model model) {
        if(adminService.hasLoggedUserPremissions("ROLE_ADMIN")==false
                && adminService.hasLoggedUserPremissions("ROLE_WORKER")==false)
            return "noPremissions";
        websiteContentService.addContent(model);
        websiteContentService.loadPageContent(model);
        return "cmsManage";
    }
}
