package zut.wi.Sportscentersmanagement.Components;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import zut.wi.Sportscentersmanagement.Entities.*;
import zut.wi.Sportscentersmanagement.Repositories.ReservationRepository;
import zut.wi.Sportscentersmanagement.Repositories.Reservation_userRepository;
import zut.wi.Sportscentersmanagement.Repositories.RoomRepository;
import zut.wi.Sportscentersmanagement.Repositories.WebsiteContentRepository;
import zut.wi.Sportscentersmanagement.Services.UserServiceManager;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ValidationComponent implements Validator {
    private boolean isEditedProfile;
    public boolean isEditedProfile() {
        return isEditedProfile;
    }
    private static final Logger LOG = Logger.getLogger(ValidationComponent.class);
    public void setEditedProfile(boolean editedProfile) {
        isEditedProfile = editedProfile;
    }
    @Autowired
    private UserServiceManager userServiceManager;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private Reservation_userRepository reservationUserRepository;
    @Autowired
    private WebsiteContentRepository websiteContentRepository;
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (user.getName().length() < 4 || user.getName().length() > 30) {
            errors.rejectValue("name", "Size.userForm.username");
            LOG.info(String.format("Validation error. Wrong username"));
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "NotEmpty");
        if (user.getSurname().length() < 4 || user.getSurname().length() > 30) {
            errors.rejectValue("surname", "Size.userForm.surname");
            LOG.info(String.format("Validation error. Wrong surname"));
        }
        if (this.isEditedProfile == false){ //don't validate nick when editing profile
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nick", "NotEmpty");
            if (user.getNick().length() < 4 || user.getNick().length() > 30) {
                errors.rejectValue("nick", "Size.userForm.nick");
                LOG.info(String.format("Validation error. Wrong nick"));
            }
            if (userServiceManager.findByNick(user.getNick()) != null) {
                errors.rejectValue("nick", "Duplicate.userForm.nick");
            }
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getEmail().length() < 4 || user.getEmail().length() > 30) {
            errors.rejectValue("email", "Size.userForm.email");
            LOG.info(String.format("Validation error. Wrong email"));
        }
        final String regex = "^(([a-zA-z]|\\d)*)+@(^[a-zA-z])*+.+[a-zA-z]*";
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(user.getEmail());
        if(!matcher.find())
            errors.rejectValue("email", "Email.userForm.notCorrect");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty");
        if (user.getEmail().length() < 4 || user.getEmail().length() > 30) {
            errors.rejectValue("address", "Size.userForm.address");
            LOG.info(String.format("Validation error. Wrong address"));
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 4 || user.getPassword().length() > 30) {
            errors.rejectValue("password", "Size.userForm.password");
            LOG.info(String.format("Validation error. Wrong password"));
        }
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
            LOG.info(String.format("Validation error. Wrong password"));
        }
    }

    public void validatePass(Pass passForm, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "NotEmpty");
        if (passForm.getType().length() < 2 || passForm.getType().length() > 10) {
            errors.rejectValue("type", "Size.passForm.type");
            LOG.info(String.format("Validation error. Wrong pass type"));
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty");
    }

    public void validateActivity(Activity activityForm, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "NotEmpty");
        if (activityForm.getType().length() < 2 || activityForm.getType().length() > 10) {
            errors.rejectValue("type", "Size.passForm.type");
            LOG.info(String.format("Validation error. Wrong activity type"));
        }
    }

    public void validateRoom(Room roomForm, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maxNumberOfPeople", "NotEmpty");
    }

    public void validateReservation(Reservation reservationForm, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reservedDate", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reservedFromHour", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reservetToHour", "NotEmpty");
        if(!errors.hasErrors()) {
            if(isRoomFree(reservationForm)==false) {
                errors.rejectValue("room", "Room.reservationForm.busy");
                errors.rejectValue("reservedDate", "Room.reservationForm.busy");
                errors.rejectValue("reservedFromHour", "Room.reservationForm.busy");
                errors.rejectValue("reservetToHour", "Room.reservationForm.busy");
            }
            if(isLeaderFree(reservationForm)==false) {
                errors.rejectValue("leader", "Leader.reservationForm.busy");
                errors.rejectValue("reservedDate", "Leader.reservationForm.busy");
                errors.rejectValue("reservedFromHour", "Leader.reservationForm.busy");
                errors.rejectValue("reservetToHour", "Leader.reservationForm.busy");
            }
        }
    }

    public boolean isLeaderFree(Reservation reservationForm) {
        Integer selectedLeaderId=reservationForm.getLeader().getId();
        Iterable<Reservation> reservations=reservationRepository.findAll();
        boolean areHoursFreeForLeader=true;
        for (Reservation actualReservation:reservations) {
            Integer actualLeaderId=actualReservation.getLeader().getId();
            Long actualReservedDate=actualReservation.getReservedDate().getTime();
            Long reservationFormDate=reservationForm.getReservedDate().getTime();
            if(actualReservedDate.equals(reservationFormDate)) {
                if (actualLeaderId.equals(selectedLeaderId)) {
                    areHoursFreeForLeader=checkHours(reservationForm,actualReservation);
                }
            }
        }
        if(areHoursFreeForLeader==true)
            return true;
        else
            return false;
    }

    private boolean isRoomFree(Reservation reservationForm) {
        Iterable<Reservation> reservations=reservationRepository.findAll();
        Integer roomIdToReservation=reservationForm.getRoom().getId();
        boolean areHoursFreeForRoom=true;
        for(Reservation actualReservation:reservations) {
            Room actualRoom=actualReservation.getRoom();
            Integer actualRoomId=actualRoom.getId();
            Long actualReservedDate=actualReservation.getReservedDate().getTime();
            Long reservationFormDate=reservationForm.getReservedDate().getTime();
            if(actualReservedDate.equals(reservationFormDate))
                if (actualRoomId.equals(roomIdToReservation)) {
                    areHoursFreeForRoom=checkHours(reservationForm,actualReservation);
                }
        }
        if(areHoursFreeForRoom==true)
            return true;
        else
            return false;
    }

    public boolean userAlreadyExist(User loggedUser,Reservation selectedReservation) {
        Iterable<Reservation_user> allReservations=reservationUserRepository.findAll();
        Integer loggedUserId=loggedUser.getId();
        boolean isDateAndTimeCollision=false;
        for (Reservation_user reservation : allReservations) {
            Integer actualReservationUserId=reservation.getUser().getId();
            if(actualReservationUserId.equals(loggedUserId)) {
                isDateAndTimeCollision=checkDateAndTimeCollision(reservation,selectedReservation);
                if(isDateAndTimeCollision==true)
                    break;
            }
        }
        if(isDateAndTimeCollision==false)
            return false;
        else
            return true;
    }

    private boolean checkDateAndTimeCollision(Reservation_user reservation, Reservation selectedReservation) {
        boolean collision=false;
        Date actualReservationDate=reservation.getReservation().getReservedDate();
        Date selectedReservationDate=selectedReservation.getReservedDate();
        if(actualReservationDate.equals(selectedReservationDate)) {
            collision= checkHours(selectedReservation,reservation);
        }
        return collision;
    }

    private boolean checkHours(Reservation reservationForm, Reservation actualReservation) {
        Date reservationFormTimeFrom=reservationForm.getReservedFromHour();
        Date reservationFormTimeTo=reservationForm.getReservetToHour();
        Date actualReservationTimeFrom=actualReservation.getReservedFromHour();
        Date actualReservationTimeTo=actualReservation.getReservetToHour();
        if((reservationFormTimeFrom.before(actualReservationTimeFrom)&& reservationFormTimeTo.before(actualReservationTimeFrom))
            || (reservationFormTimeFrom.after(actualReservationTimeTo) && reservationFormTimeTo.after(reservationFormTimeFrom)))
            return true;
        else
            return false;
    }

    private boolean checkHours(Reservation selectedReservation,Reservation_user reservation) {
        Date reservationFormTimeFrom=selectedReservation.getReservedFromHour();
        Date reservationFormTimeTo=selectedReservation.getReservetToHour();
        Date actualReservationTimeFrom=reservation.getReservation().getReservedFromHour();
        Date actualReservationTimeTo=reservation.getReservation().getReservetToHour();
        if((reservationFormTimeFrom.equals(actualReservationTimeFrom)&& reservationFormTimeTo.equals(actualReservationTimeTo))
                || (reservationFormTimeFrom.equals(actualReservationTimeFrom) && reservationFormTimeTo.equals(reservationFormTimeTo)))
            return true;
        else
            return false;
    }


    public Reservation checkIfReservationHasFreeSlots(Reservation item) {
        Integer reservationRoomMaxPeople=item.getRoom().getMaxNumberOfPeople();
        Integer reservationActualNumberUsers=item.getNumberOfUsers();
        if(reservationRoomMaxPeople.equals(reservationActualNumberUsers)){
            item.setReservationAvaliable(false);
            reservationRepository.save(item);
        }
         return item;
    }

    public void validateNewContent(WebsiteContent contentForm, String contentType, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortDescription", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longDescription", "NotEmpty");
        if(contentType.equals("NULL"))
            errors.rejectValue("contentType", "NewContent.ContentType.NotSelected");
        if((contentType.equals("slide1") ||contentType.equals("slide2")
                || contentType.equals("slide3")) && slidesExist(contentType)==true)
            errors.rejectValue("contentType", "NewContent.slider.TooManySlides");
        if((contentType.equals("news1") || contentType.equals("news2") || contentType.equals("news3"))
                &&  newsExist(contentType)==true)
            errors.rejectValue("contentType", "NewContent.news.TooManyNews");
        if((contentType.equals("footerFirst") || contentType.equals("footerSecond")) && footersExist()==true) {
            errors.rejectValue("contentType", "NewContent.footer.TooManyFooters");
        }
        if(contentType.equals("singlePageHeader") && headerExist()==true) {
            errors.rejectValue("contentType", "NewContent.footer.TooManyHeaders");
        }
    }

    private boolean headerExist() {
        Optional<WebsiteContent> singlePageHeader=websiteContentRepository.findOptionalByContentType("singlePageHeader");
        if(singlePageHeader.isPresent())
            return true;
        else
            return false;
    }

    private boolean footersExist() {
        Optional<WebsiteContent> footerFirst=websiteContentRepository.findOptionalByContentType("footerFirst");
        Optional<WebsiteContent> footerSecond=websiteContentRepository.findOptionalByContentType("footerSecond");
        if(footerFirst.isPresent() && footerSecond.isPresent())
            return true;
        else
            return false;
    }

    private boolean newsExist(String contentType) {
        Optional<WebsiteContent> news1=websiteContentRepository.findOptionalByContentType("news1");
        Optional<WebsiteContent> news2=websiteContentRepository.findOptionalByContentType("news2");
        Optional<WebsiteContent> news3=websiteContentRepository.findOptionalByContentType("news3");
        if(news1.isPresent() && news2.isPresent() && news3.isPresent())
            return true;
        if(news1.isPresent() && contentType.equals("news1"))
            return true;
        if(news2.isPresent() && contentType.equals("news2"))
            return true;
        if(news3.isPresent() && contentType.equals("news3"))
            return true;
        return false;
    }

    private boolean slidesExist(String contentType) {
        Optional<WebsiteContent> slide1=websiteContentRepository.findOptionalByContentType("slide1");
        Optional<WebsiteContent> slide2=websiteContentRepository.findOptionalByContentType("slide2");
        Optional<WebsiteContent> slide3=websiteContentRepository.findOptionalByContentType("slide3");
        if(slide1.isPresent() && slide2.isPresent() && slide3.isPresent())
            return true;
        if(slide1.isPresent() && contentType.equals("slide1"))
            return true;
        if(slide2.isPresent() && contentType.equals("slide2"))
            return true;
        if(slide3.isPresent() && contentType.equals("slide3"))
            return true;
        return false;
    }
}
