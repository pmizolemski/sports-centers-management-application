package zut.wi.Sportscentersmanagement.Components;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import zut.wi.Sportscentersmanagement.Entities.*;
import zut.wi.Sportscentersmanagement.Repositories.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@Component
public class InitData implements ApplicationRunner {
    private RoleRepository roleRepository;
    private UsersRepository usersRepository;
    private ActivityRepository activityRepository;
    private PassRepository passRepository;
    private RoomRepository roomRepository;
    private WebsiteContentRepository websiteContentRepository;
    private SettingsRepository settingsRepository;
    private static final Logger LOG = Logger.getLogger(InitData.class);
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    public InitData(RoleRepository roleRepository, UsersRepository usersRepository,
                    ActivityRepository activityRepository, PassRepository passRepository,
                    RoomRepository roomRepository,WebsiteContentRepository websiteContentRepository,
                    SettingsRepository settingsRepository) {
        this.roleRepository = roleRepository;
        this.usersRepository=usersRepository;
        this.activityRepository=activityRepository;
        this.passRepository=passRepository;
        this.roomRepository=roomRepository;
        this.websiteContentRepository=websiteContentRepository;
        this.settingsRepository=settingsRepository;
    }

    public void createRoles() {
        roleRepository.save(new Role("ROLE_ADMIN"));
        roleRepository.save(new Role("ROLE_WORKER"));
        roleRepository.save(new Role("ROLE_CLIENT"));
        LOG.info(String.format("User roles created and saved"));
    }

    public void createUsers() {
        User defaultAdmin= new User("admin","admin","admin","admin@admin.ad",
                "fjgjtg457kfde4"," ","Żeromskiego 5/5");
        Role defultRole1=roleRepository.findByName("ROLE_ADMIN");
        Set<Role> roleSet1 = new HashSet();
        roleSet1.add(defultRole1);
        defaultAdmin.setRoles(roleSet1);
        defaultAdmin.setPassword(bCryptPasswordEncoder.encode("fjgjtg457kfde4"));
        usersRepository.save(defaultAdmin);

        User defaultTrainer1= new User("pudzian","Mariusz","Pudzianowski","mariusz@pudzianowski.pl",
                "pudzian"," ","Wojska Polskiego 55,2");
        Role defaultRole2=roleRepository.findByName("ROLE_WORKER");
        Set<Role> roleSet2 = new HashSet();
        roleSet2.add(defaultRole2);
        defaultTrainer1.setRoles(roleSet2);
        defaultTrainer1.setPassword(bCryptPasswordEncoder.encode("pudzian"));
        usersRepository.save(defaultTrainer1);

        User defaultTrainer2= new User("jexample","John","Example","john@example.pl",
                "jexample"," ","Wojska Polskiego 1/2");
        defaultTrainer2.setRoles(roleSet2);
        defaultTrainer2.setPassword(bCryptPasswordEncoder.encode("jexample"));
        usersRepository.save(defaultTrainer2);

        User defaultWorker1= new User("jkowalski","Janusz","Kowalski","jan@kowalski.pl",
                "jkowalski"," ","Mazowiecka 3/4");
        defaultWorker1.setRoles(roleSet2);
        defaultWorker1.setPassword(bCryptPasswordEncoder.encode("jkowalski"));
        usersRepository.save(defaultWorker1);


        Role defaultRole3=roleRepository.findByName("ROLE_CLIENT");
        Set<Role> roleSet3 = new HashSet();
        roleSet3.add(defaultRole3);
        for (Integer i=0;i<=9;i++) {
            User defaultUser= new User("user"+i.toString(),"Example","User"+i.toString(),"example@example.pl",
                    "user"+i.toString()," ","Example Address 1/2");
            defaultUser.setRoles(roleSet3);
            defaultUser.setPassword(bCryptPasswordEncoder.encode("user"));
            usersRepository.save(defaultUser);
        }
        LOG.info(String.format("Users created and saved"));
    }

    public void createActivities() {
        Activity gym=new Activity("gym","");
        activityRepository.save(gym);
        Activity yoga=new Activity("yoga","");
        activityRepository.save(yoga);
        Activity crossfit=new Activity("cross fit","");
        LOG.info(String.format("Users creared and saved"));
        activityRepository.save(crossfit);
        LOG.info(String.format("Activities created and saved"));
    }

    public void createPasses() {
        Pass gym=new Pass("gym",75.0f,"");
        passRepository.save(gym);
        Pass sauna=new Pass("sauna",40.0f,"");
        passRepository.save(sauna);
        Pass pool=new Pass("pool",79.99f,"");
        passRepository.save(pool);
        Pass trainer=new Pass("trainer",50.0f,"");
        passRepository.save(trainer);
        LOG.info(String.format("Activities created and saved"));
    }

    public void createWebsiteContent() {
        WebsiteContent box1 = new WebsiteContent("box", "short description short description short description short description", "long description long description long description long description", "news.jpg", "b1", "box 1");
        websiteContentRepository.save(box1);
        WebsiteContent box2 = new WebsiteContent("box", "short description short description short description short description", "long description long description long description long description", "news0.jpg", "b2", "box 2");
        websiteContentRepository.save(box2);
        WebsiteContent box3 = new WebsiteContent("box", "short description short description short description short description", "long description long description long description long description", "news5.jpg", "b3", "box 3");
        websiteContentRepository.save(box3);
        WebsiteContent footerFirst = new WebsiteContent("footerFirst", "some quick text", "&nbsp", "", "f1", "f1");
        websiteContentRepository.save(footerFirst);
        WebsiteContent footerSecond = new WebsiteContent("footerSecond", "yourDomain.com    All rights reserved", "yourDomain.com    All rights reserved", "", "f2", "f2");
        websiteContentRepository.save(footerSecond);
        WebsiteContent menuItem1 = new WebsiteContent("menuItem", "About us", "About us About us About us About us About us ", "", "about", "About us");
        websiteContentRepository.save(menuItem1);
        WebsiteContent menuItem2 = new WebsiteContent("menuItem", "Workers", "Workers Workers Workers Workers Workers ", "", "workers", "Workers");
        websiteContentRepository.save(menuItem2);
        WebsiteContent menuItem3 = new WebsiteContent("menuItem", "Passes", "Passes Passes Passes Passes Passes ", "", "passes", "Passes");
        websiteContentRepository.save(menuItem3);
        WebsiteContent menuItem4 = new WebsiteContent("menuItem", "lorem ipsum lorem", "lorem ipsum lorem ipsum lorem ipsum ", "", "contact", "Contact");
        websiteContentRepository.save(menuItem4);
        WebsiteContent news1 = new WebsiteContent("news1", "lorem ipsum lorem", "lorem ipsum lorem ipsum lorem ipsum ", "news1.jpg", "n1", "News 1");
        websiteContentRepository.save(news1);
        WebsiteContent news2 = new WebsiteContent("news2", "lorem ipsum lorem", "lorem ipsum lorem ipsum lorem ipsum ", "news5.jpg", "n2", "News 2");
        websiteContentRepository.save(news2);
        WebsiteContent news3 = new WebsiteContent("news3", "lorem ipsum lorem", "lorem ipsum lorem ipsum lorem ipsum ", "news6.jpg", "n3", "News 3");
        websiteContentRepository.save(news3);
        WebsiteContent singlePageHeader = new WebsiteContent("singlePageHeader", "Company name", "some quick description", "", "h1", "h1");
        websiteContentRepository.save(singlePageHeader);
        WebsiteContent slide1 = new WebsiteContent("slide1", "x1 s1 s1 ", "...", "man.jpg", "s1", "slide 1");
        websiteContentRepository.save(slide1);
        WebsiteContent slide2 = new WebsiteContent("slide2", "s2 s2 s2", "dfdf dd d", "woman.jpg", "s2", "slide 2");
        websiteContentRepository.save(slide2);
        WebsiteContent slide3 = new WebsiteContent("slide3", "s3 s3 s3", "dfdf dd d", "man.jpg", "s3", "slide 3");
        websiteContentRepository.save(slide3);
        LOG.info(String.format("Website Content created and saved"));
    }

    public void createRooms() {
        Room room1=new Room(1,30,"Room number 1 with space for 30 people");
        roomRepository.save(room1);
        Room room2=new Room(2,10,"");
        roomRepository.save(room2);
        Room room3=new Room(3,15,"");
        roomRepository.save(room3);
        Room room4=new Room(4,20,"");
        roomRepository.save(room4);
        LOG.info(String.format("Rooms created and saved"));
    }
    public boolean isFirstRunApplication() {
        Optional<Settings> tryGetDefaultSettings=settingsRepository.findById(1);
        if (tryGetDefaultSettings.isPresent())
            return false;
        else
            return true;
    }
    public void run(ApplicationArguments args) { //for save init data to database
        if (isFirstRunApplication()==true) {
            LOG.info(String.format("First run application detected. Starting to create init data..."));
            createRoles();
            createUsers();
            createActivities();
            createPasses();
            createWebsiteContent();
            createRooms();
            Settings defaultSettings = new Settings(1, false);
            settingsRepository.save(defaultSettings);
        }
    }
}