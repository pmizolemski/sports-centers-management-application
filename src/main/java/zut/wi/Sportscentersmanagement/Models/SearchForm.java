package zut.wi.Sportscentersmanagement.Models;
public class SearchForm {
    private String surname;
    private String nick;

    public SearchForm() {
    }

    public SearchForm(String surname, String nick) {
        this.surname = surname;
        this.nick = nick;
    }

    public String getNick() {
        return nick;
    }

    public String getSurname() {
        return surname;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
