package zut.wi.Sportscentersmanagement.Services;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import zut.wi.Sportscentersmanagement.Entities.WebsiteContent;
import zut.wi.Sportscentersmanagement.Repositories.WebsiteContentRepository;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
public class WebsiteContentService {
    @Autowired
    private WebsiteContentRepository websiteContentRepository;
    @Autowired
    ServletContext context;
    private static final Logger LOG = Logger.getLogger(WebsiteContentService.class);

    public void addContent(Model model, WebsiteContent contentForm, String contentType, MultipartFile file) {
        LOG.info(String.format("FUNCTION BODY: addOrUpdateContent"));
        String uploadedFileName=tryToUploadFile(file);
        Integer id=contentForm.getId();
        Optional<WebsiteContent> content=websiteContentRepository.findById(id);
        if(content.isPresent())
            updateContent(model, contentForm,contentType,uploadedFileName);
        else {
            WebsiteContent newContent = new WebsiteContent();
            newContent.setContentType(contentType);
            newContent.setImageName(uploadedFileName);
            newContent.setLongDescription(contentForm.getLongDescription());
            newContent.setShortDescription(contentForm.getShortDescription());
            newContent.setTitle(contentForm.getTitle());
            newContent.setUrl(contentForm.getUrl());
            websiteContentRepository.save(newContent);
        }
            model.addAttribute("isContentCreatedOrEdit", true);
            showCMS(model);
    }

    private String tryToUploadFile(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            String absolutePath = context.getRealPath("uploads/");
            Path path = Paths.get(absolutePath + file.getOriginalFilename());
            String fileName=file.getOriginalFilename();
            Files.write(path, bytes);
            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void showCMS(Model model) {
        LOG.info(String.format("FUNCTION BODY: showCMS"));
        Iterable<WebsiteContent> contents=websiteContentRepository.findAll();
        model.addAttribute("contents",contents);
    }

    public void showContent(Integer contentId,Model model) {
        LOG.info(String.format("FUNCTION BODY: showContent"));
        WebsiteContent contentToShow=websiteContentRepository.findById(contentId).get();
        model.addAttribute("contentForm",contentToShow);
        model.addAttribute("isEditOrAddMode",true);
        showCMS(model);
    }

    public void deleteContent(Model model, Integer contentId) {
        LOG.info(String.format("FUNCTION BODY: deleteContent"));
        websiteContentRepository.deleteById(contentId);
        model.addAttribute("isContentDeleted",true);
        showCMS(model);
    }

    public void addContent(Model model) {
        LOG.info(String.format("FUNCTION BODY: addContent"));
        model.addAttribute("isEditOrAddMode",true);
        WebsiteContent contentForm=new WebsiteContent();
        model.addAttribute("contentForm",contentForm);
        showCMS(model);
    }

    public void updateContent(Model model, WebsiteContent contentForm, String contentType,String uploadedFilePath) {
        LOG.info(String.format("FUNCTION BODY: updateContent"));
        WebsiteContent contentToEdit=websiteContentRepository.findById(contentForm.getId());
        contentToEdit.setContentType(contentForm.getContentType());
        contentToEdit.setImageName(uploadedFilePath);
        contentToEdit.setLongDescription(contentForm.getLongDescription());
        contentToEdit.setShortDescription(contentForm.getShortDescription());
        contentToEdit.setTitle(contentForm.getTitle());
        contentToEdit.setUrl(contentForm.getUrl());
        contentToEdit.setId(contentForm.getId());
        websiteContentRepository.save(contentToEdit);
    }
    public void loadMainMenu(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadMainMenu"));
        Iterable<WebsiteContent> mainMenuItems=websiteContentRepository.findAllByContentType("menuItem");
        model.addAttribute("mainMenuItems",mainMenuItems);
    }

    public void loadBoxes(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadBoxes"));
        Iterable<WebsiteContent> boxesList=websiteContentRepository.findAllByContentType("box");
        model.addAttribute("boxesList",boxesList);
    }

    public void loadFooter(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadFooter"));
        Optional<WebsiteContent> optionalFooterFirst=websiteContentRepository.findOptionalByContentType("footerFirst");
        Optional<WebsiteContent> optionalFooterSecond=websiteContentRepository.findOptionalByContentType("footerSecond");
        if(optionalFooterFirst.isPresent()){
            WebsiteContent footerFirst=optionalFooterFirst.get();
            model.addAttribute("footerFirst",footerFirst);
            model.addAttribute("footerFirstEnabled",true);
        }
        if(optionalFooterSecond.isPresent()){
            WebsiteContent footerSecond=optionalFooterSecond.get();
            model.addAttribute("footerSecond",footerSecond);
            model.addAttribute("footerSecondtEnabled",true);
        }
    }

    public void loadSlider(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadSlider"));
        Optional<WebsiteContent> OptionalSlide1=websiteContentRepository.findOptionalByContentType("slide1");
        Optional<WebsiteContent> OptionalSlide2=websiteContentRepository.findOptionalByContentType("slide2");
        Optional<WebsiteContent> OptionalSlide3=websiteContentRepository.findOptionalByContentType("slide3");
        if(OptionalSlide1.isPresent() && OptionalSlide2.isPresent() && OptionalSlide3.isPresent()) {
            WebsiteContent slide1=OptionalSlide1.get();
            model.addAttribute("slide1", slide1);
            WebsiteContent slide2=OptionalSlide2.get();
            model.addAttribute("slide2", slide2);
            WebsiteContent slide3=OptionalSlide3.get();
            model.addAttribute("slide3", slide3);
            model.addAttribute("sliderEnabled", true);
        }
        else
            model.addAttribute("sliderEnabled", false);
    }

    public void loadNews(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadNews"));
        Optional<WebsiteContent> OptionalNews1=websiteContentRepository.findOptionalByContentType("news1");
        Optional<WebsiteContent> OptionalNews2=websiteContentRepository.findOptionalByContentType("news2");
        Optional<WebsiteContent> OptionalNews3=websiteContentRepository.findOptionalByContentType("news3");
        if(OptionalNews1.isPresent()) {
            WebsiteContent news1=OptionalNews1.get();
            model.addAttribute("news1",news1);
            model.addAttribute("news1Enabled",true);
        }
        if(OptionalNews2.isPresent()) {
            WebsiteContent news2=OptionalNews2.get();
            model.addAttribute("news2",news2);
            model.addAttribute("news2Enabled",true);
        }
        if(OptionalNews3.isPresent()) {
            WebsiteContent news3=OptionalNews3.get();
            model.addAttribute("news3",news3);
            model.addAttribute("news3Enabled",true);
        }
    }

    public void loadPageContent(Model model) {
        LOG.info(String.format("FUNCTION BODY: loadPageContent"));
        WebsiteContent singlePageHeader=websiteContentRepository.findByContentType("singlePageHeader");
        Iterable<WebsiteContent> leftMenuItems=websiteContentRepository.findAllByContentType("menuItem");
        model.addAttribute("singlePageHeader",singlePageHeader);
        model.addAttribute("leftMenuItems",leftMenuItems);
        loadFooter(model);
    }
}
