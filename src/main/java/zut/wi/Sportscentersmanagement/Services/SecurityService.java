package zut.wi.Sportscentersmanagement.Services;

public interface SecurityService {
    String findLoggedInUsername();
    void autologin(String username, String password);
}
