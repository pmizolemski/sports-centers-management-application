package zut.wi.Sportscentersmanagement.Services;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import zut.wi.Sportscentersmanagement.Entities.*;
import zut.wi.Sportscentersmanagement.Models.SearchForm;
import zut.wi.Sportscentersmanagement.Repositories.*;
import java.util.*;

@Service
public class AdminService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserServiceManager userServiceManager;
    @Autowired
    private PassRepository passRepository;
    @Autowired
    private Pass_userRepository pass_userRepository;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private Reservation_userRepository reservation_userRepository;
    private static final Logger LOG = Logger.getLogger(AdminService.class);

    public void deleteUser(Integer id) {
        LOG.info(String.format("FUNCTION BODY: deleteUser"));
        usersRepository.deleteById(id);
    }

    public void searchUser(SearchForm searchForm, Model model) {
        LOG.info(String.format("FUNCTION BODY: searchUser"));
        ArrayList<User> results=new ArrayList<>();
        String searchedNick = searchForm.getNick();
        String searchedSurname = searchForm.getSurname();
        LOG.info(String.format("SEARCHED NICK:"+searchedNick, " SEARCHED SURNAME:"+searchedSurname));
        User searchedUserByNick=usersRepository.findByNick(searchedNick);
        Iterable<User> searchedUsersBySurname=usersRepository.findAllBySurname(searchedSurname);
        if(searchedUserByNick!=null && searchedUsersBySurname!=null) {
            searchedUsersBySurname.forEach(item-> {
                if(searchedUserByNick.getId()!=item.getId())
                    results.add(item);
            });
        }
        else if (searchedUsersBySurname!=null) {
            searchedUsersBySurname.forEach(item-> {
                    results.add(item);
            });
        }
        if (searchedUserByNick!=null) {
            results.add(searchedUserByNick);
        }
        model.addAttribute("searchedUser",results);

    }

    public void showPremissions(Model model) {
        LOG.info(String.format("FUNCTION BODY: showPremissions"));
        List<Role> avaliableRoles=roleRepository.findAll();
        model.addAttribute("avaliableRoles", avaliableRoles);
        Iterable<User> usersList = userServiceManager.findAll();
        model.addAttribute("usersList", usersList);
        model.addAttribute("isRoleUpdated",false);
    }

    public void updateRole(Integer userId, Integer roleId, Model model) {
        LOG.info(String.format("FUNCTION BODY: updateRole"));
        User userToUpdateRole= userServiceManager.findById(userId);
        Role roleToSet=new Role();
        Optional<Role> optionalRole=roleRepository.findById(roleId);
        if(optionalRole.isPresent())
            roleToSet=optionalRole.get();
        Set<Role> roleSet = new HashSet();
        roleSet.add(roleToSet);
        userToUpdateRole.setRoles(roleSet);
        usersRepository.save(userToUpdateRole);
        showPremissions(model);
        model.addAttribute("isRoleUpdated",true);
    }

    public void managePasses(Model model) {
        LOG.info(String.format("FUNCTION BODY: managePasses"));
        List<Pass> avaliablePasses=passRepository.findAll();
        model.addAttribute("avaliablePasses", avaliablePasses);
        model.addAttribute("isPassEditMode",false);
        model.addAttribute("passWasEdited",false);
        model.addAttribute("passWasEdited",false);
        model.addAttribute("isPassDeleted",false);
    }

    public void editPass(Pass passForm, Model model) {
        LOG.info(String.format("FUNCTION BODY: editPass"));
        Pass passToEdit=passRepository.findById(passForm.getId());
        if(passToEdit==null)
            passToEdit=new Pass();
        if(passForm.getDescription()==null)
            passForm.setDescription(" ");
        passToEdit.setDescription(passForm.getDescription());
        passToEdit.setPrice(passForm.getPrice());
        passToEdit.setType(passForm.getType());
        passRepository.save(passToEdit);
        model.addAttribute("passWasEdited",true);
        managePasses(model);
        model.addAttribute("isPassEditMode",false);
        model.addAttribute("passWasEdited",true);
        model.addAttribute("isPassDeleted",false);
    }

    public void showPassToEdit(Integer passId, Model model) {
        LOG.info(String.format("FUNCTION BODY: showPassToEdit"));
        Pass passToEdit=passRepository.findById(passId).get();
        model.addAttribute("passForm",passToEdit);
        model.addAttribute("isPassEditMode",true);
        model.addAttribute("passWasEdited",false);
        model.addAttribute("passWasEdited",false);
        model.addAttribute("isPassDeleted",false);
    }

    public void deletePass(Integer id,Model model) {
        LOG.info(String.format("FUNCTION BODY: deletePass"));
        passRepository.deleteById(id);
        managePasses(model);
        model.addAttribute("isPassDeleted",true);
    }

    public void addPass(Model model) {
        LOG.info(String.format("FUNCTION BODY: addPass"));
        Pass newPass= new Pass();
        model.addAttribute("passForm",newPass);
        model.addAttribute("isPassEditMode",true);
        List<Pass> avaliablePasses=passRepository.findAll();
        model.addAttribute("avaliablePasses", avaliablePasses);
    }

    public void manageUsersPasses(Model model) {
        LOG.info(String.format("FUNCTION BODY: manageUsersPasses"));
        Iterable<Pass_user> allPasses=pass_userRepository.findAll();
        ArrayList<Pass_user> passesNotPaid=new ArrayList<>();
        allPasses.forEach(item->{
            if(item.isWasPaid()==false) {
                passesNotPaid.add(item);
            }
        });
        model.addAttribute("passesNotPaid",passesNotPaid);
        Iterable<User> usersList = userServiceManager.findAll();
        model.addAttribute("usersList", usersList);
    }

    public void showUser(Integer userId, Model model) {
        LOG.info(String.format("FUNCTION BODY: showUser"));
        User userToEdit=userServiceManager.findById(userId);
        userToEdit.setPassword("");
        model.addAttribute("userForm", userToEdit);
        model.addAttribute("isUserDeleted", true);
    }

    public void updateUserProfile(User userForm,Integer userId) {
        LOG.info(String.format("FUNCTION BODY: updateUserProfile"));
        User userToUpdate=userServiceManager.findById(userId);
        userToUpdate.setPassword(userForm.getPassword());
        userToUpdate.setPasswordConfirm("");
        userToUpdate.setAddress(userForm.getAddress());
        userToUpdate.setEmail(userForm.getEmail());
        userToUpdate.setName(userForm.getName());
        userToUpdate.setSurname(userForm.getSurname());
        userServiceManager.setUserProfileEdited(true);
        userServiceManager.save(userToUpdate);
    }

    public void showAllUsers(Model model) {
        Iterable<User> usersList = userServiceManager.findAll();
        model.addAttribute("usersList", usersList);
        model.addAttribute("searchForm",new SearchForm("",""));
    }

    public void deleteUser(Integer id, Model model) {
        deleteUser(id);
        model.addAttribute("isUserDeleted", true);
        showAllUsers(model);
    }

    public void showPayments(Model model) {
        LOG.info(String.format("FUNCTION BODY: showPayments"));
        Iterable<Pass_user> allPasses= pass_userRepository.findAll();
        ArrayList<Pass_user> actualUsersOrders=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        Date dateNow=calendar.getTime();
        calendar.setTime(dateNow);
        allPasses.forEach(item->{
            if(calendar.getTime().before(item.getExpirationTime())) {
                actualUsersOrders.add(item);
            }
            else {
                userServiceManager.deleteOrderPass(item.getId());
            }
        });
        ArrayList<Pass_user> paidPasses= new ArrayList<>();
        ArrayList<Pass_user> notPaidPasses=new ArrayList<>();
        actualUsersOrders.forEach(pass->{
            if(pass.isWasPaid()==true)
                paidPasses.add(pass);
            else
                notPaidPasses.add(pass);
        });
        model.addAttribute("paidPasses",paidPasses);
        model.addAttribute("notPaidPasses",notPaidPasses);
    }

    public void deletePayment(Integer paymentId, Model model) {
        pass_userRepository.deleteById(paymentId);
        LOG.info(String.format("FUNCTION BODY: deletePayment"));
        showPayments(model);
    }

    public void confirmPayment(Integer paymentId, Model model) {
        Pass_user paymentToConfirm=pass_userRepository.findById(paymentId).get();
        paymentToConfirm.setWasPaid(true);
        pass_userRepository.save(paymentToConfirm);
        showPayments(model);
    }

    public void setPass(Model model) {
        Iterable<User> users=usersRepository.findAll();
        model.addAttribute("usersList",users);
        Iterable<Pass> passes=passRepository.findAll();
        model.addAttribute("passes",passes);
    }

    public void manageActivities(Model model) {
        LOG.info(String.format("FUNCTION BODY: manageActivities"));
        List<Activity> avaliableActivities=activityRepository.findAll();
        model.addAttribute("avaliableActivities", avaliableActivities);
        model.addAttribute("isActivityEditMode",false);
        model.addAttribute("ActivityWasEdited",false);
        model.addAttribute("ActivityWasEdited",false);
        model.addAttribute("isActivityDeleted",false);
    }

    public void editActivity(Activity activityForm, Model model) {
        LOG.info(String.format("FUNCTION BODY: editActivity"));
        Activity activityToEdit=activityRepository.findById(activityForm.getId());
        if(activityToEdit==null)
            activityToEdit=new Activity();
        if(activityForm.getDescription()==null)
            activityForm.setDescription(" ");
        activityToEdit.setDescription(activityForm.getDescription());
        activityToEdit.setType(activityForm.getType());
        activityRepository.save(activityToEdit);
        model.addAttribute("activityWasEdited",true);
        managePasses(model);
        model.addAttribute("isActivityEditMode",false);
        model.addAttribute("activityWasEdited",true);
        model.addAttribute("isActivityDeleted",false);
    }

    public void showActivityToEdit(Integer activityId, Model model) {
        LOG.info(String.format("FUNCTION BODY: showActivityToEdit"));
        Activity activityToEdit=activityRepository.findById(activityId).get();
        model.addAttribute("activityForm",activityToEdit);
        model.addAttribute("isActivityEditMode",true);
        model.addAttribute("activityWasEdited",false);
        model.addAttribute("activityWasEdited",false);
        model.addAttribute("isActivityDeleted",false);
    }

    public void deleteActivity(Integer id,Model model) {
        LOG.info(String.format("FUNCTION BODY: deleteActivity"));
        activityRepository.deleteById(id);
        manageActivities(model);
        model.addAttribute("isActivityDeleted",true);
    }

    public void addActivity(Model model) {
        LOG.info(String.format("FUNCTION BODY: addActivity"));
        Activity newActivity= new Activity();
        model.addAttribute("activityForm",newActivity);
        model.addAttribute("isActivityEditMode",true);
        List<Activity> avaliableActivities=activityRepository.findAll();
        model.addAttribute("avaliableActivities", avaliableActivities);

    }

    public void manageRooms(Model model) {
        LOG.info(String.format("FUNCTION BODY: manageRooms"));
        List<Room> avaliableRooms=roomRepository.findAll();
        model.addAttribute("avaliableRooms", avaliableRooms);
        model.addAttribute("isRoomEditMode",false);
        model.addAttribute("RoomWasEdited",false);
        model.addAttribute("RoomWasEdited",false);
        model.addAttribute("isRoomDeleted",false);
    }

    public void editRoom(Room roomForm, Model model) {
        LOG.info(String.format("FUNCTION BODY: editRoom"));
        Room roomToEdit=roomRepository.findById(roomForm.getId());
        if(roomToEdit==null)
            roomToEdit=new Room();
        if(roomForm.getDescription()==null)
            roomForm.setDescription(" ");
        roomToEdit.setDescription(roomForm.getDescription());
        roomToEdit.setMaxNumberOfPeople(roomForm.getMaxNumberOfPeople());
        roomToEdit.setNumber(roomForm.getNumber());
        roomRepository.save(roomToEdit);
        model.addAttribute("RoomWasEdited",true);
        managePasses(model);
        model.addAttribute("isRoomEditMode",false);
        model.addAttribute("roomWasEdited",true);
        model.addAttribute("isRoomDeleted",false);
    }

    public void showRoomToEdit(Integer roomId, Model model) {
        LOG.info(String.format("FUNCTION BODY: showRoomToEdit"));
        Room roomToEdit=roomRepository.findById(roomId).get();
        model.addAttribute("roomForm",roomToEdit);
        model.addAttribute("isRoomEditMode",true);
        model.addAttribute("roomWasEdited",false);
        model.addAttribute("roomWasEdited",false);
        model.addAttribute("isRoomDeleted",false);
    }

    public void deleteRoom(Integer id,Model model) {
        LOG.info(String.format("FUNCTION BODY: deleteRoom"));
        roomRepository.deleteById(id);
        manageRooms(model);
        model.addAttribute("isRoomDeleted",true);
    }

    public void addRoom(Model model) {
        LOG.info(String.format("FUNCTION BODY: addRoom"));
        Room newActivity= new Room();
        model.addAttribute("roomForm",newActivity);
        model.addAttribute("isRoomEditMode",true);
        List<Room> avaliableRooms=roomRepository.findAll();
        model.addAttribute("avaliableRooms", avaliableRooms);

    }

    public void manageReservations(Model model) {
        LOG.info(String.format("FUNCTION BODY: manageReservations"));
        List<Reservation> allReservations=reservationRepository.findAll();
        Calendar calendar=Calendar.getInstance();
        Date dateNow=calendar.getTime();
        calendar.setTime(dateNow);
        allReservations.forEach(item->{
            if(calendar.getTime().before(item.getReservedDate())) {}
            else {
                userServiceManager.deleteOutOfDateReservation(item.getId(),model);
            }
        });
        List<Reservation> allAvaliableReservations=reservationRepository.findAll();
        model.addAttribute("avaliableReservations", allAvaliableReservations);
        model.addAttribute("isReservationEditMode",false);
        model.addAttribute("ReservationWasEdited",false);
        model.addAttribute("ReservationWasEdited",false);
        model.addAttribute("isReservationDeleted",false);
    }

    public void editReservation(Reservation reservationForm, Model model) {
        LOG.info(String.format("FUNCTION BODY: editReservation"));
        Reservation reservationToEdit=reservationRepository.findById(reservationForm.getId());
        if(reservationToEdit==null)
            reservationToEdit=new Reservation();
        reservationToEdit.setActivityType(reservationForm.getActivityType());
        reservationToEdit.setLeader(reservationForm.getLeader());
        reservationToEdit.setReservedDate(reservationForm.getReservedDate());
        reservationToEdit.setReservedFromHour(reservationForm.getReservedFromHour());
        reservationToEdit.setReservetToHour(reservationForm.getReservetToHour());
        reservationToEdit.setRoom(reservationForm.getRoom());
        reservationToEdit.setReservationAvaliable(true);
        reservationToEdit.setNumberOfUsers(0);
        reservationRepository.save(reservationToEdit);
        model.addAttribute("ReservationWasEdited",true);
        manageReservations(model);
        model.addAttribute("isReservationEditMode",false);
        model.addAttribute("reservationWasEdited",true);
        model.addAttribute("isReservationDeleted",false);
        List<Reservation> avaliableReservations=reservationRepository.findAll();
        model.addAttribute("avaliableReservation", avaliableReservations);
    }

    public void deleteReservation(Integer id,Model model) {
        LOG.info(String.format("FUNCTION BODY: deleteReservation"));
        Reservation reservationToDelete=reservationRepository.findById(id).get();
        Iterable<Reservation_user> reservations_UserToDelete=reservation_userRepository.findAllByReservation(reservationToDelete);
        reservations_UserToDelete.forEach(reservation_user -> {
            reservation_userRepository.deleteById(reservation_user.getId());
        });
        reservationRepository.deleteById(id);
        manageReservations(model);
        model.addAttribute("isReservationDeleted",true);
    }


    public void addReservation(Model model) {
        LOG.info(String.format("FUNCTION BODY: addReservation"));
        Reservation newReservation= new Reservation();
        model.addAttribute("reservationForm",newReservation);
        model.addAttribute("isReservationEditMode",true);
        loadResources(model);
    }

    public void loadResources(Model model) {
        List<Reservation> avaliableReservations=reservationRepository.findAll();
        model.addAttribute("avaliableReservations", avaliableReservations);
        Iterable<Room> rooms=roomRepository.findAll();
        model.addAttribute("rooms", rooms);
        Iterable<Activity> activities=activityRepository.findAll();
        model.addAttribute("activities", activities);
        Iterable<User> users=usersRepository.findAll();
        ArrayList<User> leaders=new ArrayList<>();
        Role userRole=roleRepository.findByName("ROLE_WORKER");
        users.forEach(user-> {
            Iterable<Role> actualUserRoles=user.getRoles();
            actualUserRoles.forEach(actualRole-> {
                if(actualRole.getName().equals(userRole.getName()))
                    leaders.add(user);
            });

        });
        model.addAttribute("leaders", leaders);
    }

    public boolean hasLoggedUserPremissions(String searchedRole) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick=authentication.getName();
        User loggedUser=userServiceManager.findByNick(loggedUserNick);
        Role role=roleRepository.findByName(searchedRole);
        Iterable<Role> loggedUserRoles=loggedUser.getRoles();
        for (Role actualRole: loggedUserRoles) {
            if(actualRole.getName().equals(role.getName()))
                return true;
        }
        return false;
    }
}
