package zut.wi.Sportscentersmanagement.Services;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import zut.wi.Sportscentersmanagement.Entities.WebsiteContent;
import zut.wi.Sportscentersmanagement.Repositories.WebsiteContentRepository;

@Service
public class HomeService {
    @Autowired
    WebsiteContentRepository websiteContentRepository;
    private static final Logger LOG = Logger.getLogger(WebsiteContentService.class);
    public void loadFullPage(Model model, String page) {
        LOG.info(String.format("FUNCTION BODY: loadFullPage"));
        WebsiteContent pageContent=websiteContentRepository.findByUrl(page);
        model.addAttribute("pageContent",pageContent);
    }
}
