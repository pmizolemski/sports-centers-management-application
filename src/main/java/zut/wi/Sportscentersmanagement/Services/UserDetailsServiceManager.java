package zut.wi.Sportscentersmanagement.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zut.wi.Sportscentersmanagement.Entities.Role;
import zut.wi.Sportscentersmanagement.Entities.User;
import zut.wi.Sportscentersmanagement.Repositories.UsersRepository;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceManager implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = usersRepository.findByNick(username);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new org.springframework.security.core.userdetails.User(user.getNick(), user.getPassword(), grantedAuthorities);
    }
}