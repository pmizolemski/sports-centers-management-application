package zut.wi.Sportscentersmanagement.Services;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import zut.wi.Sportscentersmanagement.Components.ValidationComponent;
import zut.wi.Sportscentersmanagement.Entities.*;
import zut.wi.Sportscentersmanagement.Repositories.*;
import java.util.*;

@Service
public class UserServiceManager {
    private boolean isUserProfileEdited=false;
    @Autowired
    private UserServiceManager userServiceManager;
    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PassRepository passRepository;
    @Autowired
    private Pass_userRepository pass_userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private Reservation_userRepository reservation_userRepository;
    @Autowired
    private ValidationComponent validationComponent;

    private static final Logger LOG = Logger.getLogger(UserServiceManager.class);
    public void setUserProfileEdited(boolean userProfileEdited) {
        isUserProfileEdited = userProfileEdited;
    }

    public void save(User user) {
        LOG.info(String.format("FUNCTION BODY: save, service:  UserServiceManager"));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setPasswordConfirm("");
        if(this.isUserProfileEdited==false) {
            Role defultRole = roleRepository.findByName("ROLE_CLIENT");
            Set<Role> roleSet = new HashSet();
            roleSet.add(defultRole);
            user.setRoles(roleSet);
        }
        userRepository.save(user);
    }

    public void updateUserProfile(User userForm) {
        LOG.info(String.format("FUNCTION BODY: updateUserProfile, service:  UserServiceManager"));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick=authentication.getName();
        userForm.setNick(loggedUserNick);
        User userToUpdate=userServiceManager.findByNick(loggedUserNick);
        userToUpdate.setNick(loggedUserNick);
        userToUpdate.setPassword(userForm.getPassword());
        userToUpdate.setPasswordConfirm("");
        userToUpdate.setAddress(userForm.getAddress());
        userToUpdate.setEmail(userForm.getEmail());
        userToUpdate.setName(userForm.getName());
        userToUpdate.setSurname(userForm.getSurname());
        userServiceManager.setUserProfileEdited(true);
        userServiceManager.save(userToUpdate);
    }

    public User findByNick(String username) {
        LOG.info(String.format("FUNCTION BODY: findByNick, service:  UserServiceManager"));
        return userRepository.findByNick(username);
    }

    public User findById(int id) {
        LOG.info(String.format("FUNCTION BODY: findById, service:  UserServiceManager"));
        return userRepository.findById(id);
    }

    public Iterable<User> findAll() {
        LOG.info(String.format("FUNCTION BODY: findAll, service:  UserServiceManager"));
        return userRepository.findAll();
    }

    public User showLoggedUserProfile() {
        LOG.info(String.format("FUNCTION BODY: showLoggedUserProfile, service:  UserServiceManager"));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick=authentication.getName();
        User loggedUser = userServiceManager.findByNick(loggedUserNick);
        loggedUser.setPassword("");
        loggedUser.setPasswordConfirm("");
        return loggedUser;
    }

    public void showAvaliablePasses(Model model) {
        LOG.info(String.format("FUNCTION BODY: showAvaliablePasses, service:  UserServiceManager"));
        Iterable<Pass> avaliablePasses=passRepository.findAll();
        model.addAttribute("avaliablePasses",avaliablePasses);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick=authentication.getName();
        Integer userId =userServiceManager.findByNick(loggedUserNick).getId();
        Iterable<Pass_user> allUserOrders=pass_userRepository.findAllByUserId(userId);
        ArrayList<Pass_user> actualUserOrders=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        Date dateNow=calendar.getTime();
        calendar.setTime(dateNow);
        allUserOrders.forEach(item->{
            if(calendar.getTime().before(item.getExpirationTime()))
                actualUserOrders.add(item);
            else
                deleteOrderPass(item.getId());
        });
        model.addAttribute("actualUserOrders",actualUserOrders);
        model.addAttribute("userMode",true);
    }

    public void orderPass(Integer passId, String userNick, Model model, boolean adminMode) {
        LOG.info(String.format("FUNCTION BODY: orderPass, service:  UserServiceManager"));
        User user =userServiceManager.findByNick(userNick);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick = authentication.getName();
        User loggedUser = userServiceManager.findByNick(loggedUserNick);
        if(userNick.equals(loggedUser.getNick()) || adminMode) {
            Pass selectedPass=passRepository.findById(passId).get();
            model.addAttribute("userMode",true);
            if(passExist(model, selectedPass, user)==false)
                saveOrder(model,selectedPass,user);
        }
        else
            model.addAttribute("orderFail",true);
    }

    private void saveOrder(Model model, Pass selectedPass, User user) {
        Pass_user pass_user=new Pass_user();
        pass_user.setPass(selectedPass);
        pass_user.setUser(user);
        pass_user.setWasPaid(false);
        Calendar calendar=Calendar.getInstance();
        Date dateNow=calendar.getTime();
        calendar.setTime(dateNow);
        calendar.add(calendar.DATE,30);
        pass_user.setExpirationTime(calendar.getTime());
        pass_userRepository.save(pass_user);
        Iterable<Pass_user> actualUserOrders=pass_userRepository.findAllByUserId(user.getId());
        model.addAttribute("actualUserOrders",actualUserOrders);
        model.addAttribute("userMode",true);
        showAvaliablePasses(model);
    }

    private boolean passExist(Model model,Pass selectedPass, User user) {
        Iterable<Pass_user> allUserPasses=pass_userRepository.findAllByUserId(user.getId());
        for(Pass_user pass:allUserPasses) {
            if(selectedPass.getType().equals(pass.getPass().getType())) {
                model.addAttribute("orderPassError",true);
                showAvaliablePasses(model);
                Iterable<Pass_user> actualUserOrders=pass_userRepository.findAllByUserId(user.getId());
                model.addAttribute("actualUserOrders",actualUserOrders);
                LOG.info(String.format("FUNCTION BODY: passExist, service:  UserServiceManager, returned value: true"));
                return true;
            }
        }
        return false;
    }

    public void deleteOrderPass(Integer orderId, Model model) {
        LOG.info(String.format("FUNCTION BODY: deleteOrderPass, service:  UserServiceManager"));
        pass_userRepository.deleteById(orderId);
        showAvaliablePasses(model);
    }

    public void deleteOrderPass(Integer orderId) {
        LOG.info(String.format("FUNCTION BODY: deleteOrderPass, service:  UserServiceManager"));
        pass_userRepository.deleteById(orderId);
    }

    public void updateUser(Model model) {
        LOG.info(String.format("FUNCTION BODY: updateUser, service:  UserServiceManager"));
        User loggedUser=userServiceManager.showLoggedUserProfile();
        model.addAttribute("userForm", loggedUser);
        model.addAttribute("isUserDeleted", true);
    }

    public void login(String error, Model model,String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");
        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
    }

    public void reservation(Model model) {
        LOG.info(String.format("FUNCTION BODY: reservation, service:  UserServiceManager"));
        List<Reservation> allReservations=reservationRepository.findAll();
        ArrayList<Reservation> avaliableReservationList=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        Date dateNow=calendar.getTime();
        calendar.setTime(dateNow);
        allReservations.forEach(item->{
            if(calendar.getTime().before(item.getReservedDate())) {
                Reservation reservation=validationComponent.checkIfReservationHasFreeSlots(item);
                avaliableReservationList.add(reservation);
            }
            else
                deleteOutOfDateReservation(item.getId(),model);
        });
        Collections.sort(avaliableReservationList,Comparator.comparing(Reservation::getReservedDate));
        model.addAttribute("reservationsList",avaliableReservationList);
        showActualUserReservations(model);
    }

    private void showActualUserReservations(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick = authentication.getName();
        User loggedUser = userServiceManager.findByNick(loggedUserNick);
        Iterable<Reservation_user> actualUserReservations=reservation_userRepository.findAllByUser(loggedUser);
        model.addAttribute("actualUserReservations",actualUserReservations);
    }

    public void deleteOutOfDateReservation(int id, Model model) {
        LOG.info(String.format("FUNCTION BODY: deleteUnactualReservation, service:  UserServiceManager"));
        Reservation reservationToDelete=reservationRepository.findById(id);
        Iterable<Reservation_user> reservations_UserToDelete=reservation_userRepository.findAllByReservation(reservationToDelete);
        reservations_UserToDelete.forEach(reservation_user -> {
            reservation_userRepository.deleteById(reservation_user.getId());
        });
        reservationRepository.deleteById(id);
        LOG.info(String.format("FUNCTION BODY: reservation, service:  UserServiceManager"));
        reservation(model);
    }


    public void bookReservation(Model model,Integer bookId) {
        LOG.info(String.format("FUNCTION BODY: bookReservation, service:  UserServiceManager"));
        Reservation selectedReservation=reservationRepository.findById(bookId).get();
        Room selectedReservationRoom=selectedReservation.getRoom();
        Integer actualNumberOfUsers=selectedReservation.getNumberOfUsers();
        Integer newNumberOfUsers=actualNumberOfUsers+1;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick = authentication.getName();
        User loggedUser = userServiceManager.findByNick(loggedUserNick);
        if(newNumberOfUsers>selectedReservationRoom.getMaxNumberOfPeople()) {
            model.addAttribute("tooManyUsers",true);
            selectedReservation.setReservationAvaliable(false);
            List<Reservation> allReservations=reservationRepository.findAll();
            model.addAttribute("reservationsList",allReservations);
        }
        else if(validationComponent.userAlreadyExist(loggedUser,selectedReservation)==true) {
            model.addAttribute("userExist",true);
            reservation(model);
        }
        else {
            Reservation_user newReservation = new Reservation_user();
            newReservation.setUser(loggedUser);
            newReservation.setReservation(selectedReservation);
            selectedReservation.setNumberOfUsers(newNumberOfUsers);
            reservationRepository.save(selectedReservation);
            reservation_userRepository.save(newReservation);
            reservation(model);
            model.addAttribute("reservationSuccess",true);
        }
    }

    public void deleteBookReservation(Model model, Integer reservationId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedUserNick = authentication.getName();
        User loggedUser = userServiceManager.findByNick(loggedUserNick);
        Iterable<Reservation_user> userReservations=reservation_userRepository.findAllByUser(loggedUser);
        userReservations.forEach(actualReservation-> {
            Integer actualReservationId=actualReservation.getId();
            if(reservationId.equals(actualReservationId)) {
                Reservation mainReservation=reservationRepository.findById(actualReservation.getReservation().getId());
                Integer mainReservationActualNumerOfUsers=mainReservation.getNumberOfUsers();
                Integer newNumberOfUsers=mainReservationActualNumerOfUsers-1;
                mainReservation.setReservationAvaliable(true);
                mainReservation.setNumberOfUsers(newNumberOfUsers);
                reservationRepository.save(mainReservation);
                reservation_userRepository.deleteById(reservationId);
            }
        });
        reservation(model);
    }
}
