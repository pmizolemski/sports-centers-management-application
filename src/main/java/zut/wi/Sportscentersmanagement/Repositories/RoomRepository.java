package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Room;

public interface RoomRepository extends JpaRepository<Room, Integer> {
    Room findById(int id);
}