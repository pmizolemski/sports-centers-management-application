package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.WebsiteContent;

import java.util.Optional;

public interface WebsiteContentRepository extends JpaRepository<WebsiteContent, Integer> {
    WebsiteContent findById(int id);
    Iterable<WebsiteContent> findAllByContentType(String type);
    WebsiteContent findByContentType(String type);
    WebsiteContent findByUrl(String name);
    Optional<WebsiteContent> findOptionalByContentType(String type);
}
