package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Pass;

public interface PassRepository extends JpaRepository<Pass, Integer> {
    Pass findById(int id);
}