package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Pass;
import zut.wi.Sportscentersmanagement.Entities.Reservation;
import zut.wi.Sportscentersmanagement.Entities.Role;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    Reservation findById(int id);
}