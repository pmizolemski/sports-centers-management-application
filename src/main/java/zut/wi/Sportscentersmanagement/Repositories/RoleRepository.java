package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Role;
import zut.wi.Sportscentersmanagement.Entities.User;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}

