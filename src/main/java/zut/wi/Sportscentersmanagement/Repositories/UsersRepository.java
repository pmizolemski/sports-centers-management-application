package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zut.wi.Sportscentersmanagement.Entities.User;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {
    User findByNick(String nick);
    User findById(int id);
    Iterable<User> findAllBySurname(String Surname);
}

