package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Pass_user;
import zut.wi.Sportscentersmanagement.Entities.Reservation;
import zut.wi.Sportscentersmanagement.Entities.Reservation_user;
import zut.wi.Sportscentersmanagement.Entities.User;

public interface Reservation_userRepository extends JpaRepository<Reservation_user, Integer> {
    Pass_user findById(int id);
    Iterable<Reservation_user> findAllByUser(User user);
    Iterable<Reservation_user> findAllByReservation(Reservation reservation);
}
