package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Activity;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    Activity findById(int id);
}
