package zut.wi.Sportscentersmanagement.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import zut.wi.Sportscentersmanagement.Entities.Pass;
import zut.wi.Sportscentersmanagement.Entities.Pass_user;
import zut.wi.Sportscentersmanagement.Entities.Role;

public interface Pass_userRepository extends JpaRepository<Pass_user, Integer> {
    Pass_user findById(int id);
    Iterable<Pass_user> findAllByUserId(int id);
}