<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <div class="adminArea">
                    <%@ include file="layout/userMenu.jsp" %>
                    <c:if test="${isEditOrAddMode==true}">
                        <form:form method="POST" modelAttribute="contentForm" class="px-4 py-3 loginFormSinglePage" action="/cms/content/edit" enctype="multipart/form-data">
                            <spring:bind path="id">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="hidden" path="id" class="form-control" placeholder="enter id"
                                                autofocus="true"></form:input>
                                    <form:errors path="id" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="title">
                                <label for="exampleDropdownFormEmail1">Title</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="text" path="title" class="form-control" placeholder="enter title"
                                                autofocus="true"></form:input>
                                    <form:errors path="title" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="contentType">
                                <label for="exampleDropdownFormEmail1">contentType</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <select name="contentType" path="contentType">
                                             <option value="NULL">-- SELECT --</option>
                                            <option value="slide1">slide1</option>
                                            <option value="slide2">slide2</option>
                                            <option value="slide3">slide3</option>
                                            <option value="news1">news1</option>
                                            <option value="news2">news2</option>
                                            <option value="news3">news3</option>
                                            <option value="box">box</option>
                                            <option value="footerFirst">footerFirst</option>
                                            <option value="footerSecond">footerSecond</option>
                                            <option value="menuItem">menuItem</option>
                                            <option value="singlePageHeader">singlePageHeader</option>
                                    </select>
                                    <form:errors path="contentType" cssStyle="color:red;"></form:errors>
                            </spring:bind>
                            <spring:bind path="shortDescription">
                                <label for="exampleDropdownFormEmail1">Short description</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:textarea cols="10" rows="10" path="shortDescription" class="form-control" placeholder="enter short description"
                                                autofocus="true"></form:textarea>
                                    <br />
                                    <form:errors path="shortDescription" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="longDescription">
                                <label for="exampleDropdownFormEmail1">Long description</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:textarea cols="10" rows="10" path="longDescription" class="form-control" placeholder="enter longDescription"
                                                autofocus="true"></form:textarea>
                                    <form:errors path="longDescription" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="imageName">
                                <label for="exampleDropdownFormEmail1">image</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="hidden" path="imageName" class="form-control" placeholder="enter image"
                                                autofocus="true"></form:input>
                                    <form:errors path="imageName" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                                  <input type="file" name="file" /><br/><br/>
                            <spring:bind path="url">
                                <label for="exampleDropdownFormEmail1">url</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="text" path="url" class="form-control" placeholder="enter url"
                                                autofocus="true"></form:input>
                                    <form:errors path="url" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
                        </form:form>
                    </c:if>
                    <c:if test="${isContentDeleted==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            Content was deleted.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${isContentCreatedOrEdit==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            Content was saved.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                </div>
                    <a href="/cms/content/add"> <button class="btn btn-lg btn-primary btn-blockAdmin">add</button> </a> <br /> <br />
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col">Title</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="content" items="${contents}">
                        <tr>
                            <td>${content.contentType}</td>
                            <td>${content.title}</td>
                            <td>
                                <a href="/cms/deleteContent/${content.id}"> <button class="btn btn-lg btn-danger btn-blockAdmin">delete</button> </a>
                                &nbsp <a href="/cms/showContent/${content.id}"> <button class="btn btn-lg btn-secondary btn-blockAdmin">show</button> </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br />
            </div>
        </div>
    </div>
</div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>