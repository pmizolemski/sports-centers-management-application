<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
</div>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <form:form method="POST" modelAttribute="userForm" class="px-4 py-3 loginFormSinglePage">
                    <h2 class="form-signin-heading">Create your account</h2>
                    <spring:bind path="name">
                        <div class="form-group ${status.error ? 'has-error' : ''}" >
                            <label for="exampleDropdownFormEmail1">Name</label>
                            <form:input type="text" path="name" class="form-control" placeholder="name"
                                        autofocus="true"></form:input>
                            <form:errors path="name" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="surname">
                        <label for="exampleDropdownFormEmail1">Surname</label>
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <form:input type="text" path="surname" class="form-control" placeholder="surname"
                                        autofocus="true"></form:input>
                            <form:errors path="surname" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="nick">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="exampleDropdownFormEmail1">Nick</label>
                            <form:input type="text" path="nick" class="form-control" placeholder="nick"
                                        autofocus="true"></form:input>
                            <form:errors path="nick" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="email">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="exampleDropdownFormEmail1">Email </label>
                            <form:input type="text" path="email" class="form-control" placeholder="email"
                                        autofocus="true"></form:input>
                            <form:errors path="email" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="address">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="exampleDropdownFormEmail1">Address</label>
                            <form:input type="text" path="address" class="form-control" placeholder="address"
                                        autofocus="true"></form:input>
                            <form:errors path="address" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="password">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="exampleDropdownFormEmail1">Password</label>
                            <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
                            <form:errors path="password" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <spring:bind path="passwordConfirm">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label for="exampleDropdownFormEmail1">Comfirm password</label>
                            <form:input type="password" path="passwordConfirm" class="form-control"
                                        placeholder="Confirm your password"></form:input>
                            <form:errors path="passwordConfirm" cssStyle="color:red;"></form:errors>
                        </div>
                    </spring:bind>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form:form>
            </div>
        </div>
    </div>
</div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>