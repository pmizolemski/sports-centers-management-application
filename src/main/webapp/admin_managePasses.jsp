<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <div class="adminArea">
                    <%@ include file="layout/userMenu.jsp" %>
                        <c:if test="${isPassEditMode==true}">
                            <form:form method="POST" modelAttribute="passForm" class="px-4 py-3 loginFormSinglePage" action="/admin/pass/edit/">
                                <spring:bind path="id">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <form:input type="hidden" path="id" class="form-control" placeholder="enter id"
                                                    autofocus="true"></form:input>
                                        <form:errors path="id" cssStyle="color:red;"></form:errors>
                                    </div>
                                </spring:bind>
                                <spring:bind path="type">
                                    <label for="exampleDropdownFormEmail1">Type</label>
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <form:input type="text" path="type" class="form-control" placeholder="enter type"
                                                    autofocus="true"></form:input>
                                        <form:errors path="type" cssStyle="color:red;"></form:errors>
                                    </div>
                                </spring:bind>
                                <spring:bind path="price">
                                    <label for="exampleDropdownFormEmail1">Price</label>
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="0.01" path="price" class="form-control" placeholder="enter price"
                                                    autofocus="true"></form:input>
                                        <form:errors path="price" cssStyle="color:red;"></form:errors>
                                    </div>
                                </spring:bind>
                                <spring:bind path="description">                                    <label for="exampleDropdownFormEmail1">Description</label>
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <form:input type="text" path="description" class="form-control" placeholder="enter price"
                                                    autofocus="true"></form:input>
                                        <form:errors path="description" cssStyle="color:red;"></form:errors>
                                    </div>
                                </spring:bind>
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
                            </form:form>
                        </c:if>
                    <c:if test="${passWasEdited==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            Pass was updated.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${isPassDeleted==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            Pass was deleted.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                </div>
                <br />
                <a href="/admin/pass/add/"> <button class="btn btn-lg btn-primary btn-blockAdmin" style="margin-bottom: 4px;right:0px;">Add</button> </a>
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Type</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="pass" items="${avaliablePasses}">
                        <tr>
                            <td>${pass.id}</td>
                            <td>${pass.type}</td>
                            <td>${pass.price}</td>
                            <td>
                                <a href="/admin/pass/edit/${pass.id}"> <button class="btn btn-lg btn-secondary btn-blockAdmin" style="margin-bottom: 4px;">Edit</button> </a>
                                &nbsp <a href="/admin/pass/delete/${pass.id}"> <button class="btn btn-lg btn-danger btn-blockAdmin" style="margin-bottom: 4px;">Delete</button> </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>