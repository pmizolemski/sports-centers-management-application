<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <div class="adminArea">
                    <%@ include file="layout/userMenu.jsp" %>
                </div>
                <c:if test="${orderPassError==true}">
                    <br />
                    <div class="alert alert-danger  alert-dismissible fade show" role="alert" style="margin-top: 15px;">
                        You have this pass in orders list!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </c:if>
                <c:if test="${orderFail==true}">
                    <br />
                    <div class="alert alert-danger  alert-dismissible fade show" role="alert" style="margin-top: 15px;">
                        Sorry, you do not have premissions!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </c:if>
                <br /><br />
                <c:if test="${userMode==true}">
                    Actual orders list<hr>
                        <table class="table table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">was paid</th>
                                <th scope="col">Expiration time</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="order" items="${actualUserOrders}">
                                <tr>
                                    <td>${order.pass.type}</td>
                                    <td>${order.wasPaid}</td>
                                    <td>${order.expirationTime}</td>
                                    <td>
                                        <c:if test="${order.wasPaid=='false'}">
                                            <a href="/user/pass/delete/${order.id}"> <button class="btn btn-lg btn-danger btn-blockAdmin" style="margin-bottom: 4px;">cancel</button> </a><br />
                                        </c:if>
                                        <c:if test="${order.wasPaid=='true'}">
                                            <a href=""> <button class="btn btn-lg btn-danger btn-blockAdmin" style="margin-bottom: 4px;" disabled="true">cancel</button> </a><br />
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                </c:if>
                    <br /><br />Avaliable passes<hr>
                    <c:forEach var="pass" items="${avaliablePasses}">
                        <div class="card" style="width: 15rem;float:left; margin-right: 8px;">
                            <img class="card-img-top" src="${contextPath}/static/img/news/news3.jpg" width="30%" height="30%" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">${pass.type}<br /></h5>
                                <p class="card-text">Price:&nbsp &nbsp <i>${pass.price} zł</i></p>
                                <a href="/user/pass/order/${pass.id}/${pageContext.request.userPrincipal.name}">
                                    <button class="btn btn-lg btn-success btn-blockAdmin" style="margin-bottom: 4px;">Order</button>
                                </a>
                            </div>
                        </div>
                    </c:forEach>
            </div>
        </div>
    </div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>