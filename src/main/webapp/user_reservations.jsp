<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <div class="adminArea">
                    <%@ include file="layout/userMenu.jsp" %>
                    <c:if test="${reservationSuccess==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            Success!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${userExist==true}">
                        <br /><br />
                        <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                            Reservadion fail. You already take part in this event!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${tooManyUsers==true}">
                        <br /><br />
                        <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                            Too many users!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                </div>
                <br />
                You taking part on this reservations:<hr>
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col">Date</th>
                        <th scope="col">Hours</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="reservationUser" items="${actualUserReservations}">
                        <tr>
                            <td>${reservationUser.reservation.activityString()}</td>
                            <td><fmt:formatDate pattern="dd.MM.yyyy" value="${reservationUser.reservation.reservedDate}" /></td>
                            <td><fmt:formatDate pattern="kk:mm" value="${reservationUser.reservation.reservedFromHour}" /> - <fmt:formatDate pattern="kk:mm" value="${reservationUser.reservation.reservetToHour}" /></td>
                            <td>
                                <a href="/user/delete/booked/${reservationUser.id}"> <button class="btn btn-lg btn-danger btn-blockAdmin">cancel</button> </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br />
                Avaliable reservations: <hr>
                <c:forEach var="reservation" items="${reservationsList}">
                    <div class="card" style="margin-bottom: 28px;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <h3 style="text-align: left;"><span class="badge badge-danger"><fmt:formatDate pattern="dd" value="${reservation.reservedDate}" /></span></h3>
                                    <h5 style="text-align: left;"><fmt:formatDate pattern="MMM" value="${reservation.reservedDate}" /></h5>
                                </div>
                                <div class="col-sm-8" style="text-align: center;">
                                    <h3> ${reservation.activityType.type} </h3>
                                    <img src="${contextPath}/static/img/calendar.png" /> <fmt:formatDate pattern="EEEE" value="${reservation.reservedDate}" /> &nbsp;&nbsp; | &nbsp;&nbsp; <img src="${contextPath}/static/img/time.png" /> <fmt:formatDate pattern="kk:mm" value="${reservation.reservedFromHour}" /> - <fmt:formatDate pattern="kk:mm" value="${reservation.reservetToHour}" /><br />
                                   <br />
                                    <c:if test="${reservation.isReservationAvaliable()==true}">
                                        <p style="color: green"><b>Free slots!</b>  &nbsp&nbsp&nbsp <a href="/user/reservation/book/${reservation.id}" style="left: 0px;right: 0px;"> <button class="btn btn-lg btn-success btn-blockAdmin" style="margin-bottom: 4px;">book</button> </a></p>
                                    </c:if>
                                    <c:if test="${reservation.isReservationAvaliable()==false}">
                                        <p style="color: gray">Unfortunately, no free slots!  &nbsp&nbsp&nbsp <a href="/user/reservation/book/${reservation.id}" style="left: 0px;right: 0px;"> <button class="btn btn-lg btn-secondary btn-blockAdmin" style="margin-bottom: 4px;" disabled="disabled">book</button> </a></p>
                                    </c:if>
                                </div>
                                <div class="col-sm-2"  style="text-align: center;">
                                    <img src="${contextPath}/static/img/leader.png" /> <i>Leader:</i><br />${reservation.leader.name} ${reservation.leader.surname}
                                </div>
                            </div><!--row-->
                        </div>
                    </div><!--card-->
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>