<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

    <c:if test="${not empty pageContext.request.userPrincipal}">
        <c:if test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">
            Welcome <b>${pageContext.request.userPrincipal.name}</b>, <i> this is admin account.</i><hr>
            <a href="/"> <button class="btn btn-lg btn-success btn-blockAdmin">Home</button> </a>
            <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Manage
                    </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item black" href="/admin/manageUsers"> users </a>
                    <a class="dropdown-item black" href="/admin/premissions">Premissions </a>
                    <a class="dropdown-item black" href="/admin/passes">Passes </a>
                    <a class="dropdown-item black" href="/admin/activities">Manage activities </a>
                    <a class="dropdown-item black" href="/admin/rooms">Manage rooms </a>
                    <a class="dropdown-item black" href="/admin/reservations">Manage reservations </a>
                    <a class="dropdown-item black" href="/cms/show">Website content </a>
                </div>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Payemnts & passes
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item black" href="/admin/pass/payments/">Payments </a>
                    <a class="dropdown-item black" href="/admin/setPass">set Passes </a>
                </div>
            </div>
            <a href="/user/reservations"> <button class="btn btn-lg btn-info btn-blockAdmin">Activities timetable</button> </a>
            <a class="btn btn-lg btn-info btn-blockAdmin" onclick="document.forms['logoutForm'].submit()">Logout</a>
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </c:if>
        <c:if test="${pageContext.request.isUserInRole('ROLE_WORKER')}">
            Welcome <b>${pageContext.request.userPrincipal.name}</b>, <i> this is worker account.</i><hr>
            <a href="/"> <button class="btn btn-lg btn-success btn-blockAdmin">Home</button> </a>
            <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Manage
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item black" href="/admin/manageUsers"> users </a>
                    <a class="dropdown-item black" href="/admin/activities">Manage activities </a>
                    <a class="dropdown-item black" href="/admin/reservations">Manage reservations </a>
                    <a class="dropdown-item black" href="/cms/show">Website content </a>
                </div>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Payemnts & passes
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item black" href="/admin/pass/payments/">Payments </a>
                    <a class="dropdown-item black" href="/admin/setPass">set Passes </a>
                </div>
            </div>
            <a href="/user/reservations"> <button class="btn btn-lg btn-info btn-blockAdmin">Activities timetable</button> </a>
            <a class="btn btn-lg btn-info btn-blockAdmin" onclick="document.forms['logoutForm'].submit()">Logout</a>
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </c:if>
        <c:if test="${pageContext.request.isUserInRole('ROLE_CLIENT')}">
            Welcome <b>${pageContext.request.userPrincipal.name}</b>, <i> this is standard account.</i><hr>
            <a href="/"> <button class="btn btn-lg btn-info btn-blockAdmin">Home</button> </a>
            &nbsp <a href="/user/update"> <button class="btn btn-lg btn-info btn-blockAdmin">Edit profile</button> </a>
            &nbsp <a href="/user/passes"> <button class="btn btn-lg btn-info btn-blockAdmin">Passes</button> </a>
            &nbsp <a href="/user/reservations"> <button class="btn btn-lg btn-info btn-blockAdmin">Activities timetable</button> </a>
            <a class="btn btn-lg btn-info btn-blockAdmin" onclick="document.forms['logoutForm'].submit()">Logout</a>
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

        </c:if>
    </c:if>