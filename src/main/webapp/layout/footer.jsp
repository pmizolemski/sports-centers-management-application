<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container" style="padding-top:10px;padding-bottom:10px; text-align: center">
        <div class="row">
                <div class="col-md-12 footerSinglePage">
                        ${footerFirst.shortDescription}<br />
                        ${footerFirst.longDescription}
                </div>
        </div><!--row-->
</div><!--container-->
<div class="card-header card-header-footer" style="text-align: center"><p>${footerSecond.shortDescription}</p></div>