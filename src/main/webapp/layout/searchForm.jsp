<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <form:form method="POST" modelAttribute="searchForm" class="px-4 py-3 loginFormSinglePage" action="/admin/searchUser">
        <h2 class="form-signin-heading">Search:</h2>
        <spring:bind path="surname">
            <div class="form-group ${status.error ? 'has-error' : ''}" >
            <form:input type="text" path="surname" class="form-control" placeholder="search by surname"
                        autofocus="true"></form:input>
            <form:errors path="surname" cssStyle="color:red;"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="nick">
            <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="text" path="nick" class="form-control" placeholder="search by nick"
                        autofocus="true"></form:input>
            <form:errors path="nick" cssStyle="color:red;"></form:errors>
            </div>
        </spring:bind>
        <button class="btn btn-lg btn-primary btn-block" type="submit">search</button>
    </form:form>