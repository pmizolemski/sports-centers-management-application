<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <nav>
        <ul class="menuSinglePage navbar-nav mr-auto mt-2 mt-lg-0 menuSinglePage" style="display: flex;text-align:left;">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            <c:forEach var="item" items="${leftMenuItems}">
                <li class="nav-item">
                    <a class="nav-link" href="/page/${item.url}">${item.title}</a>
                </li>
            </c:forEach>
        </ul>
    </nav>