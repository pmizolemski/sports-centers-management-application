<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">${singlePageHeader.shortDescription}</h1>
        <p class="lead">${singlePageHeader.longDescription}</p>
    </div>
</div>