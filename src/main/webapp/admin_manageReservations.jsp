<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:layout="http://www.ultraq.net.nz/web/thymeleaf/layout">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="${contextPath}/static/css/style.css">
    <title>Hello, world!</title>
</head>
<%@ include file="layout/header.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@ include file="layout/leftMenu.jsp" %>
        </div>
        <div class="col-md-9" style="padding-top:55px;">
            <div layout:fragment="content">
                <div class="adminArea">
                    <%@ include file="layout/userMenu.jsp" %>
                    <c:if test="${isReservationEditMode==true}">
                        <form:form method="POST" modelAttribute="reservationForm" class="px-4 py-3 loginFormSinglePage" action="/admin/reservation/edit/">
                            <spring:bind path="id">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="hidden" path="id" class="form-control" placeholder="enter id"
                                                autofocus="true"></form:input>
                                    <form:errors path="id" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="activityType">
                                <label for="exampleDropdownFormEmail1">Activity type</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <select name="activityType" path="activityType">
                                        <c:forEach items="${activities}" var="activity">
                                            <option value="${activity.id}">${activity.type}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </spring:bind>
                            <spring:bind path="room">
                                <label for="exampleDropdownFormEmail1">Room</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <select name="room" path="room">
                                            <c:forEach items="${rooms}" var="room">
                                                <option value="${room.id}">${room.number}</option>
                                            </c:forEach>
                                        </select>
                                        <form:errors path="room" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="leader">
                                <label for="exampleDropdownFormEmail1">Leader</label>
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <select name="leader" path="leader">
                                            <c:forEach items="${leaders}" var="leader">
                                                <option value="${leader.id}">${leader.name} ${leader.surname}</option>
                                            </c:forEach>
                                        </select>
                                        <form:errors path="leader" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <spring:bind path="reservedDate">
                                Reservation date
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <form:input type="date" path="reservedDate" class="form-control"
                                                autofocus="true"></form:input>
                                    <form:errors path="reservedDate" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            Reserved from hour
                            <spring:bind path="reservedFromHour">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="time" path="reservedFromHour" class="form-control"
                                                autofocus="true"></form:input>
                                    <form:errors path="reservedFromHour" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            Reserved to hour
                            <spring:bind path="reservetToHour">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <form:input type="time" path="reservetToHour" class="form-control"
                                                autofocus="true"></form:input>
                                    <form:errors path="reservetToHour" cssStyle="color:red;"></form:errors>
                                </div>
                            </spring:bind>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
                        </form:form>
                    </c:if>
                    <c:if test="${reservationWasEdited==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            reservation was updated.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${isReservationDeleted==true}">
                        <br /><br />
                        <div class="alert alert-success  alert-dismissible fade show" role="alert">
                            reservation was deleted.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </c:if>
                </div>
                <br />
                <a href="/admin/reservation/add/"> <button class="btn btn-lg btn-primary btn-blockAdmin" style="margin-bottom: 4px;right:0px;">Add</button> </a>
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Activity type</th>
                        <th scope="col">Room</th>
                        <th scope="col">Leader</th>
                        <th scope="col">Date</th>
                        <th scope="col">Hours</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="reservation" items="${avaliableReservations}">
                        <tr>
                            <td>${reservation.activityType.type}</td>
                            <td>${reservation.room.number}</td>
                            <td>${reservation.leader.name} ${reservation.leader.surname}</td>
                            <td><fmt:formatDate pattern="dd.MM.yy" value="${reservation.reservedDate}" /></td>
                            <td><fmt:formatDate pattern="kk:mm" value="${reservation.reservedFromHour}" /> - <fmt:formatDate pattern="kk:mm" value="${reservation.reservetToHour}" /></td>
                            <td>
                                &nbsp <a href="/admin/reservation/delete/${reservation.id}"> <button class="btn btn-lg btn-danger btn-blockAdmin" style="margin-bottom: 4px;">Delete</button> </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<footer>
    <%@ include file="layout/footer.jsp" %>
</footer>
<!-- Optional JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>