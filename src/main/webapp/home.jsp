<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pl"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="${contextPath}/static/css/style.css">
        <title>Title of the document</title>
    </head>
    <body>
        <header>
            <c:if test="${sliderEnabled==true}">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <ol class="carousel-indicators">
                        <li data-target="singlePage.htmlcarouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="singlePage.htmlcarouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="singlePage.htmlcarouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-1" src="${contextPath}/uploads/${slide1.imageName}?auto=yes"  />
                                <div class="carousel-caption d-none d-md-block">
                                    <h2>${slide1.shortDescription}</h2>
                                    <p>${slide1.longDescription}</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-1" src="${contextPath}/uploads/${slide2.imageName}?auto=yes"  />
                                <div class="carousel-caption d-none d-md-block">
                                    <h2>${slide2.shortDescription}</h2>
                                    <p>${slide2.longDescription}</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-1" src="${contextPath}/uploads/${slide3.imageName}?auto=yes"  />
                                <div class="carousel-caption d-none d-md-block">
                                    <h2>${slide3.shortDescription}</h2>
                                    <p>${slide3.longDescription}</p>
                                </div>
                            </div>
                    </div> <!-- carousel-inner -->
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> <!-- div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" -->
            </c:if>
        </header>
        <nav class="position navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
            <div class="container" style="padding-top:0px;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
                        <c:forEach var="item" items="${mainMenuItems}">
                            <li class="nav-item">
                                <a class="nav-link" href="/page/${item.url}">${item.title} </a>
                            </li>
                        </c:forEach>
                    </ul>
                    <c:if test="${empty pageContext.request.userPrincipal.name}">
                        <a class="nav-link" href="login.html" data-toggle="modal" data-target="#exampleModal">Login</a>
                    </c:if>
                </div><!--class="collapse navbar-collapse"-->
            </div><!--class="container" style="padding-top:0px;"-->
        </nav> <!--class="position navbar sticky-top navbar-expand-lg navbar-dark bg-dark"-->
        <div class="container">
            <!-- modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Login form</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="px-4 py-3" method="POST" action="/login">
                                <div class="form-group">
                                    <label for="exampleDropdownFormEmail1">Nick</label>
                                    <input name="username" type="text" class="form-control" placeholder="name" autofocus="true"/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleDropdownFormPassword1">Password</label>
                                    <input name="password" type="password" class="form-control" placeholder="Password"/>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </div>
                                <button type="submit" class="btn btn-primary">Log in</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <a class="dropdown-item" href="/registration" style="color: black;">Register</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end modal -->
            <div class="row">
                <div class="card-deck">
                    <div class="col-md-12" style="padding-bottom: 75px;">
                        <c:if test="${not empty pageContext.request.userPrincipal.name}">
                            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </form>
                            Welcome,<a href="/user/update" style="color: black">${pageContext.request.userPrincipal.name}</a>
                                    <a class="btn btn-info" onclick="document.forms['logoutForm'].submit()">Logout</a>
                        </c:if>
                        <c:if test="${news1Enabled==true}">
                            <div class="card bg-dark text-white">
                                <img class="card-img" src="${contextPath}/uploads/${news1.imageName}" alt="Card image">
                                <div class="card-img-overlay">
                                    <h5 class="card-title"><a href="/page/${news1.url}">${news1.title}</a></h5>
                                    <p class="card-text"><a href="/page/${news1.url}">${news1.shortDescription}</a></p>
                                </div>
                            </div>
                        </c:if>
                    </div>  <!--col-md-12 -->
                    <c:forEach var="box" items="${boxesList}">
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" src="${contextPath}/uploads/${box.imageName}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">${box.title}</h5>
                                    <p class="card-text">${box.shortDescription}.</p>
                                    <a href="/page/${box.url}" class="btn btn-secondary">Read more</a>
                                </div>
                            </div><!--class="card"-->
                        </div><!--col-md-4-->
                    </c:forEach>
                    <c:if test="${news2Enabled==true}">
                        <div class="col-md-12 card-main-page">
                            <div class="card bg-dark text-white">
                                <img class="card-img" src="${contextPath}/uploads/${news2.imageName}" alt="Card image">
                                <div class="card-img-overlay">
                                    <h5 class="card-title"><a href="/page/${news2.url}">${news2.title}</a></h5>
                                    <p class="card-text"><a href="/page/${news2.url}">${news2.shortDescription}</a></p>
                                </div>
                            </div>
                        </div>  <!--col-md-12 -->
                    </c:if>
                    <c:if test="${news3Enabled==true}">
                        <div class="col-md-12 card-main-page">
                            <div class="card bg-dark text-white">
                                <img class="card-img" src="${contextPath}/uploads/${news3.imageName}" alt="Card image">
                                <div class="card-img-overlay">
                                    <h5 class="card-title"><a href="/page/${news3.url}">${news3.title}</a></h5>
                                    <p class="card-text"><a href="/page/${news3.url}">${news3.shortDescription}</a></p>
                                </div>
                            </div>
                        </div>  <!--col-md-12 -->
                    </c:if>
                </div><!--card-deck -->
            </div><!--class="row"-->
        </div><!-- class="container"-->
        <footer>
            <%@ include file="layout/footer.jsp" %>
        </footer>
        <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
</html>